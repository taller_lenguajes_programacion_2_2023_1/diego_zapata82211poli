package com.co.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

//value object
@Getter
@ToString
public class ProductName {
    private final String value;
    public ProductName(String value) {
        Validate.notNull(value, "El producto no puede ser nulo");
        Validate.isTrue(value.length() <= 70, "El nombre del producto no puede ser mayor a 70 caracteres");
        this.value = value;
    }
}
