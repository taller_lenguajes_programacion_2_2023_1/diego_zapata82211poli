package com.co.template.market;

import com.co.template.market.domain.product.*;

public class MainTest {
    public static void main(String[] args){
        System.out.println("Hola mundo");
        try{
            ProductName productName = new ProductName("Tostadas");
            ProductPrice productPrice = new ProductPrice(100d);
            ProductCode productCode = new ProductCode(123L);
            ProductDescription productDescription = new ProductDescription("Esta es la descripcion de este producto que se acaba de crear");

            Product product = new Product(productName, productCode, productPrice, productDescription);

            System.out.println(productName);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
