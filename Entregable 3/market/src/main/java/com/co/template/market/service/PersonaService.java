package com.co.template.market.service;

import com.co.template.market.DAO.PersonaDAO;
import com.co.template.market.DTO.PersonaDTO;
import com.co.template.market.domain.persona.*;
import com.co.template.market.repository.PersonaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaService {
    private final PersonaRepository personaRepository;

    public PersonaService (PersonaRepository personaRepository) { this.personaRepository = personaRepository; }

    public PersonaDTO save(PersonaDTO personaDTO){
        PersonaDAO personaDb = this.personaRepository.findByName(personaDTO.getName());

        if (personaDb != null) throw new IllegalArgumentException("name of person is not unique");

        PersonaName personaName = new PersonaName(personaDTO.getName());
        PersonaLastName personaLastName = new PersonaLastName(personaDTO.getLastName());
        PersonaPhone personaPhone = new PersonaPhone(personaDTO.getPhone());
        PersonaNick personaNick = new PersonaNick(personaDTO.getNick());
        Persona persona = new Persona(null, personaName,personaLastName, personaPhone, personaNick);

        PersonaDAO personaForSave = PersonaDAO.fromDomain(persona);
        PersonaDAO personaDAOSaved = this.personaRepository.save(personaForSave);
        Persona personaSavedWithDomain = personaDAOSaved.toDomain();

        return personaDTO.fromDomain(personaSavedWithDomain);
    }

    public List<PersonaDTO> getAllPersons(){
        List<PersonaDAO> personaDAOS = this.personaRepository.findAll();
        List<Persona> personas = personaDAOS.stream().map(personaDAO -> personaDAO.toDomain()).toList();
        List<PersonaDTO> personaDTOS = personas.stream().map(persona -> PersonaDTO.fromDomain(persona)).toList();
        return personaDTOS;
    }

    public PersonaDTO updatePersona(PersonaDTO personaDTO){
        Persona persona = personaDTO.toDomain();
        PersonaDAO personaDAO = this.personaRepository.save(PersonaDAO.fromDomain(persona));
        return personaDTO;
    }

    public void deletePersona (String id){
        PersonaCode personaCode = new PersonaCode(Long.parseLong(id));
        Optional<PersonaDAO> personaDAOOptional = this.personaRepository.findById(personaCode.getValue());
        if (personaDAOOptional.isPresent()){
            this.personaRepository.delete(personaDAOOptional.get());
        } else{
            throw new IllegalArgumentException("Product id does not exist");
        }
    }
}
