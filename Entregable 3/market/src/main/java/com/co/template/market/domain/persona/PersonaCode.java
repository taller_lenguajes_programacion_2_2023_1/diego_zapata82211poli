package com.co.template.market.domain.persona;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class PersonaCode {
    private final Long value;

    public PersonaCode(Long value){
        Validate.notNull(value, "El codigo de la persona no puede ser nulo");
        Validate.isTrue(value !=0, "El codigo no puede ser 0");
        this.value = value;
    }
}
