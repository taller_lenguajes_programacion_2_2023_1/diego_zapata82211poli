package com.co.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class ProductPrice {
    private final Double value;

    public ProductPrice(Double value) {
        Validate.notNull(value, "El valor no puede ser nulo");
        Validate.exclusiveBetween(1L, 1000000L, value, "El valor del producto solo puede ir de 1 a 1'");
        this.value = value;
    }
}
