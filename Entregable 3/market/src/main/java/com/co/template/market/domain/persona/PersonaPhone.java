package com.co.template.market.domain.persona;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class PersonaPhone {
    private final String value;

    public PersonaPhone(String value) {
        Validate.notNull(value, "El telefono no puede ser nulo");
        Validate.isTrue(value.length() == 10, "El telefono no puede ser diferente a 10 numeros");
        this.value = value;
    }
}
