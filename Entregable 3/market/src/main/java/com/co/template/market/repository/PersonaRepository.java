package com.co.template.market.repository;

import com.co.template.market.DAO.PersonaDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<PersonaDAO, Long> {
    PersonaDAO findByName(String name);
    PersonaDAO findByLastName(String lastName);
    PersonaDAO findByPhone(String phone);

    PersonaDAO findByNick(String nick);
}
