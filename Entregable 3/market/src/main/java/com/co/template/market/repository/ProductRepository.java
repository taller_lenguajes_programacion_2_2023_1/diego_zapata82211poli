package com.co.template.market.repository;

import com.co.template.market.DAO.ProductDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductDAO, Long> {
    ProductDAO findByName(String name);
    ProductDAO findByPrice(Double price);
}
