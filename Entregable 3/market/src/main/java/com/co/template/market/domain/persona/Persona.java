package com.co.template.market.domain.persona;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Persona {
    private PersonaCode id;
    private PersonaName name;
    private PersonaLastName lastName;
    private PersonaPhone phone;
    private PersonaNick nick;

}
