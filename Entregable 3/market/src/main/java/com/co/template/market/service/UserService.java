package com.co.template.market.service;

import com.co.template.market.DAO.UserDAO;
import com.co.template.market.DTO.UserDTO;
import com.co.template.market.domain.user.*;
import com.co.template.market.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO save(UserDTO userDTO){
        UserDAO userDb = this.userRepository.findByNick(userDTO.getNick());

        if (userDb != null) throw new IllegalArgumentException("emal is already used");

        UserNick userNick = new UserNick(userDTO.getNick());
        UserPassword userPassword = new UserPassword(userDTO.getPass());
        User user = new User(null   , userNick, userPassword);

        UserDAO userForSave = UserDAO.fromDomain(user);
        UserDAO userDAOSaved = this.userRepository.save(userForSave);
        User userSavedWithDomain = userDAOSaved.toDomain();

        return UserDTO.fromDomain(userSavedWithDomain);
    }

    public List<UserDTO> getAllUsers(){
        List<UserDAO> userDAOS = this.userRepository.findAll();
        List<User> users = userDAOS.stream().map(userDAO -> userDAO.toDomain()).toList();
        List<UserDTO> userDTOS = users.stream().map(user -> UserDTO.fromDomain(user)).toList();
        return userDTOS;
    }

    public UserDTO updateUser(UserDTO userDTO){
        User user = userDTO.toDomain();
        UserDAO userDAO = this.userRepository.save(UserDAO.fromDomain(user));
        return userDTO;
    }

    public void deleteUser(String id){
        UserCode userCode = new UserCode(Long.parseLong(id));
        Optional<UserDAO> userDAO = this.userRepository.findById(userCode.getValue());
        if (userDAO.isPresent()){
            this.userRepository.delete(userDAO.get());
        }else {
            throw new IllegalArgumentException("User id dos not exist");
        }
    }
}
