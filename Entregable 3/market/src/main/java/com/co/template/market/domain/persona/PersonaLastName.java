package com.co.template.market.domain.persona;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class PersonaLastName {
    private final String value;
    public PersonaLastName(String value) {
        Validate.notNull(value, "El apellido no puede ser nulo");
        Validate.isTrue(value.length() <= 30, "El apellido no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
