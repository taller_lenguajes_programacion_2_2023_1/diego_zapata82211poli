package com.co.template.market.domain.persona;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class PersonaName {
    private final String value;
    public PersonaName(String value) {
        Validate.notNull(value, "El nombre no puede ser nulo");
        Validate.isTrue(value.length() <= 30, "El nombre no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
