package com.co.template.market.DAO;

import com.co.template.market.domain.user.*;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class UserDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nick;

    @Column
    private String pass;

    public User toDomain(){
        UserCode userCode = new UserCode(this.id);
        UserNick userNick = new UserNick(this.nick);
        UserPassword userPassword = new UserPassword(this.pass);

        User user = new User(userCode, userNick, userPassword);
        return user;
    }

    public static UserDAO fromDomain(User user){
        UserDAO userDAO = new UserDAO();
        userDAO.setNick(user.getNick().getValue());
        userDAO.setPass(user.getPass().getValue());
        Long id = user.getId() == null ? 0l : user.getId().getValue();
        userDAO.setId(id);
        return userDAO;
    }
}
