package com.co.template.market.domain.product;
import lombok.AllArgsConstructor;
import lombok.Data;
//value object ***
@Data
@AllArgsConstructor
public class Product {
    private ProductName name;
    private ProductCode id;
    private ProductPrice price;
    private ProductDescription description;
}

