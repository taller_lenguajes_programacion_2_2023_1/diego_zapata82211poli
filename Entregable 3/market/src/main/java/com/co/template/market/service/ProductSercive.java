package com.co.template.market.service;

import com.co.template.market.DAO.ProductDAO;
import com.co.template.market.DTO.ProductDTO;
import com.co.template.market.domain.product.*;
import com.co.template.market.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ProductSercive {
    private final ProductRepository productRepository;

    public ProductSercive(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDTO save(ProductDTO productDTO){
        ProductDAO productDb = this.productRepository.findByName(productDTO.getName());

        if (productDb != null) throw new IllegalArgumentException("name of product is not unique");

        ProductName productName = new ProductName(productDTO.getName());
        ProductPrice productPrice = new ProductPrice(productDTO.getPrice());
        ProductDescription productDescription = new ProductDescription(productDTO.getDescription());
        Product product = new Product(productName, null, productPrice, productDescription);

        ProductDAO productForSave = ProductDAO.fromDomain(product);
        ProductDAO productDAOSaved = this.productRepository.save(productForSave);
        Product productSavedWithDomain = productDAOSaved.toDomain();

        return productDTO.fromDomain(productSavedWithDomain);
    }

    public List<ProductDTO> getAllProducts(){
        List<ProductDAO> productDAOS = this.productRepository.findAll();
        List<Product> products = productDAOS.stream().map(productDAO -> productDAO.toDomain()).toList();
        List<ProductDTO> productDTOS = products.stream().map(product -> ProductDTO.fromDomain(product)).toList();
        return productDTOS;
    }

    public ProductDTO updateProduct(ProductDTO productDTO){
        Product product = productDTO.toDomian();
        ProductDAO productDAO = this.productRepository.save(ProductDAO.fromDomain(product));
        return productDTO;
    }

    public void deleteProduct(String id){
        ProductCode productCode = new ProductCode(Long.parseLong(id));
        Optional<ProductDAO> productDAO = this.productRepository.findById(productCode.getValue());
        if (productDAO.isPresent()){
            this.productRepository.delete(productDAO.get());
        } else{
            throw new IllegalArgumentException("Product id does not exist");
        }
    }
}
