package com.co.template.market.domain.user;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {
    private UserCode id;
    private UserNick nick;
    private UserPassword pass;
}
