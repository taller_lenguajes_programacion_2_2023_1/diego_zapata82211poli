package com.co.template.market.controllet;

import com.co.template.market.DTO.PersonaDTO;
import com.co.template.market.domain.persona.Persona;
import com.co.template.market.service.PersonaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
public class PersonaController {
    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }
    @RequestMapping(value = "/personas", method = RequestMethod.POST)
    public ResponseEntity<?> savePersona(@RequestBody PersonaDTO personaDTO){
        try {
            PersonaDTO personaResponse = this.personaService.save(personaDTO);
            return ResponseEntity.ok(personaResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value = "/personas", method = RequestMethod.GET)
    public ResponseEntity<?> getAllPersonas(){
        try {
            List<PersonaDTO> personaResponse = this.personaService.getAllPersons();
            return ResponseEntity.ok(personaResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value = "/personas", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePersona(@RequestBody PersonaDTO personaDTO){
        try {
            PersonaDTO personaResponse = this.personaService.updatePersona(personaDTO);
            return ResponseEntity.ok(personaResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value = "/personas", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePersona(@RequestParam String idPersona){
        try {
            this.personaService.deletePersona(idPersona);
            return ResponseEntity.ok("Person was deleted");
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }
}
