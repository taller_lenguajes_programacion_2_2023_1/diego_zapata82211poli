package com.co.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class ProductDescription {
    private final String value;
    public ProductDescription(String value) {
        Validate.notNull(value, "El producto no puede ser nulo");
        Validate.isTrue(value.length() <= 120, "La descripción no puede ser mayor a 120 caracteres");
        this.value = value;
    }
}
