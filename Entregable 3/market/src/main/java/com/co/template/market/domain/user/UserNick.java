package com.co.template.market.domain.user;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class UserNick {
    private final String value;

    public UserNick(String value){
        Validate.notNull(value, "El nick de la persona no puede ser nulo");
        Validate.isTrue(value.length() > 4, "El nick no puede ser menor a 5");
        this.value = value;
    }
}
