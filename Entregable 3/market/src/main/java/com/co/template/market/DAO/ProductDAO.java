package com.co.template.market.DAO;

import com.co.template.market.domain.product.*;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "products")
@Data
public class ProductDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Double price;

    @Column
    private String description;

    public Product toDomain(){
        ProductCode productCode = new ProductCode(this.id);
        ProductPrice productPrice = new ProductPrice(this.price);
        ProductName productName = new ProductName(this.name);
        ProductDescription productDescription = new ProductDescription(this.description);

        Product product = new Product(productName, productCode, productPrice, productDescription);
        return product;
    }

    public static ProductDAO fromDomain(Product product){
        ProductDAO productDAO = new ProductDAO();
        productDAO.setPrice(product.getPrice().getValue());
        productDAO.setName(product.getName().getValue());
        productDAO.setDescription(product.getDescription().getValue());
        Long id = product.getId() == null ? 0l : product.getId().getValue();
        productDAO.setId(id);
        return productDAO;
    }
}
