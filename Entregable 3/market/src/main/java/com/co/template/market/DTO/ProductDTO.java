package com.co.template.market.DTO;

import com.co.template.market.domain.product.*;
import lombok.Data;

@Data
public class ProductDTO {
    private Long id;
    private String name;
    private Double price;
    private String description;

    public Product toDomian(){
        ProductCode productCode = new ProductCode(id);
        ProductName productName = new ProductName(name);
        ProductPrice productPrice = new ProductPrice(price);
        ProductDescription productDescription = new ProductDescription(description);
        Product product = new Product(productName, productCode, productPrice, productDescription);
        return product;
    }

    public static ProductDTO fromDomain(Product product){
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId().getValue());
        productDTO.setName(product.getName().getValue());
        productDTO.setPrice(product.getPrice().getValue());
        productDTO.setDescription(product.getDescription().getValue());
        return productDTO;
    }
}
