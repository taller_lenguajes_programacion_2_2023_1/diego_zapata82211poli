package com.co.template.market.controllet;

import com.co.template.market.DTO.ProductDTO;
import com.co.template.market.service.ProductSercive;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
public class ProductController {
    private final ProductSercive productSercive;

    public ProductController(ProductSercive productSercive) {
        this.productSercive = productSercive;
    }

    @RequestMapping(value="/products", method = RequestMethod.POST)
    public ResponseEntity<?> saveProduct(@RequestBody ProductDTO productDTO){
        try{
            ProductDTO productResponse = this.productSercive.save(productDTO);
            return ResponseEntity.ok(productResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.GET)
    public ResponseEntity<?> getAll(){
        try{
            List<ProductDTO> productResponse = this.productSercive.getAllProducts();
            return ResponseEntity.ok(productResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@RequestBody ProductDTO productDTO){
        try{
            ProductDTO productResponse = this.productSercive.updateProduct(productDTO);
            return ResponseEntity.ok(productResponse);
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/products", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@RequestParam String idProduct){
        try{
            this.productSercive.deleteProduct(idProduct);
            return ResponseEntity.ok("Product was deleted");
        } catch (NullPointerException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e){
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }
}
