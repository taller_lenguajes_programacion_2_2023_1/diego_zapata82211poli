package com.co.template.market.repository;

import com.co.template.market.DAO.UserDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDAO,Long> {
    UserDAO findByNick(String nick);
    UserDAO findByPass(String pass);
}
