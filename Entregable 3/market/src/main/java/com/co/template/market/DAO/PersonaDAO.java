package com.co.template.market.DAO;

import com.co.template.market.domain.persona.*;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "personas")
@Data
public class PersonaDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String lastName;

    @Column
    private String phone;

    @Column
    private String nick;

    public Persona toDomain(){
        PersonaCode personaCode = new PersonaCode(this.id);
        PersonaName personaName = new PersonaName(this.name);
        PersonaLastName personaLastName = new PersonaLastName(this.lastName);
        PersonaPhone personaPhone = new PersonaPhone(this.phone);
        PersonaNick personaNick = new PersonaNick(this.nick);

        Persona persona = new Persona(personaCode, personaName, personaLastName, personaPhone, personaNick);
        return persona;
    }

    public static PersonaDAO fromDomain(Persona persona){
        PersonaDAO personaDAO = new PersonaDAO();
        personaDAO.setName(persona.getName().getValue());
        personaDAO.setLastName(persona.getLastName().getValue());
        personaDAO.setPhone(persona.getPhone().getValue());
        personaDAO.setNick(persona.getNick().getValue());
        Long id = persona.getId() == null ? 0l : persona.getId().getValue();
        personaDAO.setId(id);
        return personaDAO;
    }
}
