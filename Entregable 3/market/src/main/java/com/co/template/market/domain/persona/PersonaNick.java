package com.co.template.market.domain.persona;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class PersonaNick {
    private final String value;
    public PersonaNick(String value) {
        Validate.notNull(value, "El nick no puede ser nulo");
        Validate.isTrue(value.length() <= 30, "El nick no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
