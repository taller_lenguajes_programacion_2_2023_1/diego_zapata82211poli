package com.co.template.market.domain.user;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class UserPassword {
    private final String value;

    public UserPassword(String value){
        Validate.notNull(value, "La contraseña del usuario no puede ser nulo");
        Validate.isTrue(value.length() > 4, "La contraseña no puede ser menor a 5");
        this.value = value;
    }
}
