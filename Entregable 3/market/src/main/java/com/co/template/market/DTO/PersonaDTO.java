package com.co.template.market.DTO;

import com.co.template.market.domain.persona.*;
import lombok.Data;

@Data
public class PersonaDTO {
    private Long id;
    private String name;
    private String lastName;
    private String phone;
    private String nick;

    public Persona toDomain(){
        PersonaCode personaCode = new PersonaCode(id);
        PersonaName personaName = new PersonaName(name);
        PersonaLastName personaLastName = new PersonaLastName(lastName);
        PersonaPhone personaPhone = new PersonaPhone(phone);
        PersonaNick personaNick = new PersonaNick(nick);
        Persona persona = new Persona(personaCode, personaName, personaLastName, personaPhone, personaNick);
        return persona;
    }

    public static PersonaDTO fromDomain(Persona persona){
        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setId(persona.getId().getValue());
        personaDTO.setName(persona.getName().getValue());
        personaDTO.setLastName(persona.getLastName().getValue());
        personaDTO.setPhone(persona.getPhone().getValue());
        personaDTO.setNick(persona.getNick().getValue());
        return personaDTO;
    }
}
