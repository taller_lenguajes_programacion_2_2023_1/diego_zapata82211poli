package com.co.template.market.DTO;

import com.co.template.market.domain.user.*;
import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String nick;
    private String pass;

    public User toDomain(){
        UserCode userCode = new UserCode(id);
        UserNick userNick = new UserNick(nick);
        UserPassword userPassword = new UserPassword(pass);
        User user = new User(userCode, userNick, userPassword);
        return user;
    }

    public static UserDTO fromDomain(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId().getValue());
        userDTO.setNick(user.getNick().getValue());
        userDTO.setPass(user.getPass().getValue());
        return userDTO;
    }
}
