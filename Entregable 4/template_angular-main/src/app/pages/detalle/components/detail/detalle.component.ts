import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})


export class DetalleComponent implements OnInit {
  public indice: any;
  public listProducts: any;
  public producto: any;
  cantidad = 1;

  constructor(private route: ActivatedRoute, private productsService: ProductsService) { };

  ngOnInit(): void {
    this.indice = this.route.snapshot.params['id'];
    this.callProducts();
    this.producto = this.findProduct(this.indice);
    console.log(this.producto);
  }

  public callProducts() {
    this.productsService.getAllProducts().subscribe(
      data => {
        console.log("data: ", data)
        console.log(data.products)
        this.listProducts = data.products
        console.log('Este es el indice ' + this.indice);
        console.log('este es listProducts ', this.listProducts);
      }
    );
  }
  public findProduct(indice: number) {
    const product = this.listProducts.find((u: { id: any; }) => u.id == indice);
    console.log('este es product ', product)
    return (product);
  }

  aumentarCantidad() {
    this.cantidad++;
  }

  disminuirCantidad() {
    if (this.cantidad > 1) {
      this.cantidad--;
    }
  }
}

