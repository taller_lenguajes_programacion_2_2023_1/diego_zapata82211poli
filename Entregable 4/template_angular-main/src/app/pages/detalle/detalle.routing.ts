import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import { DetalleComponent } from "./components/detail/detalle.component";

const routes: Routes =[
    {
        path: '',
        component: DetalleComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class DetalleRouting { }