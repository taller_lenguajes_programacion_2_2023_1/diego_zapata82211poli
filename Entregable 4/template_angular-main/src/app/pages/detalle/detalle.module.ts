import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleRouting } from './detalle.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DetalleComponent } from './components/detail/detalle.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  declarations: [
    DetalleComponent
  ],
  imports: [
    CommonModule,
    DetalleRouting,
    SharedModule
  ]
})
export class DetalleModule { }