import { Component, OnInit, NgModule, NgModuleRef } from '@angular/core';
import { NgForm, FormBuilder, NgModel, FormsModule, Validators, FormControl, FormGroup } from '@angular/forms';
import { UserRegisterService } from 'src/app/core/services/userRegister/user-register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  errorService: string | undefined;
  formModal: any;

  constructor(
    private _userRegister: UserRegisterService
  ) { }

  public registerForm: FormGroup = new FormGroup({
    firstName: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ]+$'),
      Validators.minLength(3),
    ]),
    lastName: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ]+$'),
      Validators.minLength(5),
      Validators.maxLength(25)]
    ),
    email: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'),
    ]),
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(25)]),
    confirm: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(25)]),
  });

  public errorMessages = {
    'firstName': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El campo es alfabetico.' },
      { type: 'minLength', message: 'Minimo es de 3 caracteres.' },
      { type: 'maxLength', message: 'Maximo es de 25 caracteres.' },
    ],
    'lastName': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El campo es alfabetico.' },
      { type: 'minLength', message: 'Minimo es de 3 caracteres.' },
    ],
    'email': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El correo está mal digitado.' },
    ],
    'password': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'minLength', message: 'Minimo es de 5 caracteres.' },
      { type: 'maxLength', message: 'Maximo es de 25 caracteres.' },
    ],
    'confirm': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'minLength', message: 'Minimo es de 5 caracteres.' },
      { type: 'maxLength', message: 'Maximo es de 25 caracteres.' },
    ],
  }

  ngOnInit(): void {

  }
  public submit(): void {
    this._userRegister.registerUser(this.registerForm.value).then(() => {
    this.registerForm.reset();
    })
  }
}

