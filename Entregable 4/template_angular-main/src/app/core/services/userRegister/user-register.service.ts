import { Injectable } from '@angular/core';
import { UserService } from 'src/app/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserRegisterService {
  constructor(private readonly registerService: UserService) {}

  async registerUser(userInfo: any) {
    const userNick = userInfo.email;
    const userName = userInfo.firstName + " " + userInfo.lastName;
    if (userInfo.password === userInfo.confirm) {
      const userPass = userInfo.password;

      this.registerService.saveUser({nick: userNick, name: userName, pass: userPass})
      .subscribe({
        next: (backResponse) => {
          //logic
          alert('user saved with success');
          console.log('backResponse success', backResponse)
        },
        error: (error) => {
          console.log('backResponse Failed', error);
          alert('backResponse Failed ' + error.error);
        }
      });
    } else {
      alert('Las contraseñas no son iguales');
    }
  }
}
