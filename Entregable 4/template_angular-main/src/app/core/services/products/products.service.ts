import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as data from './data.json';
import { of, Observable } from 'rxjs';

@Injectable()
export class ProductsService {

  constructor(
	) { }

  public getAllProducts(): Observable<any>{
		const json = JSON.stringify(data);
		return of(data);
	}
}
