import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { AuthService } from './core/services/auth/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { ProductsService } from './core/services/products/products.service';
import { RegisterComponent } from './pages/register/components/regis/register.component';
import { UserRegisterService } from './core/services/userRegister/user-register.service';
import { DetalleComponent } from './pages/detalle/components/detail/detalle.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    ProductsService,
    UserRegisterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
