import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterModel } from './shared/models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly url = "http://localhost:9001/market/users";
  constructor(private readonly http: HttpClient) { }

  saveUser(register: RegisterModel): Observable<RegisterModel> {
    return this.http.post<RegisterModel>(this.url, {nick: register.nick, pass: register.pass});
  }

  getAll(){
    return this.http.get<RegisterModel[]>(this.url);
  }
}
