import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { data } from 'src/shared/data/data';

@Injectable({
  providedIn: 'root'
})
export class RickMortyApiService {
  //private readonly urlRick = 'https://rickandmortyapi.com/api/character';

  constructor(private readonly http: HttpClient) { }

  // getAllCharacters(): Observable<any> {
  //   return this.http.get(this.urlRick);
  // }

  // getAllCharacter() {
  //   return data.results;
  // }
}
