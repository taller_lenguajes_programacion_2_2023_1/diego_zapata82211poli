var content='<div class="ui-page" deviceName="iphone11pro" deviceType="mobile" deviceWidth="375" deviceHeight="812">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devMobile devIOS iphone-device canvas firer commentable non-processed" alignment="left" name="Template 1" width="375" height="812">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677126671198.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-97d2b188-bcf3-4297-ab84-fd191db0eec5" class="screen growth-vertical devMobile devIOS iphone-device canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Filtros" width="375" height="812">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/97d2b188-bcf3-4297-ab84-fd191db0eec5-1677126671198.css" />\
      <div class="freeLayout">\
      <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Buscar" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Button_8" class="button multiline manualfit firer commentable non-processed" customid="Button "   datasizewidth="312.0px" datasizeheight="55.0px" dataX="31.5" dataY="713.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_8_0">Buscar</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Hotspot_2" class="imagemap firer click ie-background commentable non-processed" customid="Buscar"   datasizewidth="312.0px" datasizeheight="55.0px" dataX="31.5" dataY="713.0"  >\
          <div class="clickableSpot"></div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Precio" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Min" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_1" class="email text firer ie-background commentable non-processed" customid="Input"  datasizewidth="121.8px" datasizeheight="48.0px" dataX="53.7" dataY="495.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="email"  value="" maxlength="100"  tabindex="-1" placeholder="Precio min."/></div></div>  </div></div></div>\
          <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="72.5px" datasizeheight="28.0px" dataX="53.5" dataY="467.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_4_0">Minimo</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Max" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_2" class="email text firer ie-background commentable non-processed" customid="Input"  datasizewidth="121.8px" datasizeheight="48.0px" dataX="228.2" dataY="495.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="email"  value="" maxlength="100"  tabindex="-1" placeholder="Precio max."/></div></div>  </div></div></div>\
          <div id="s-Paragraph_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="72.5px" datasizeheight="28.0px" dataX="228.0" dataY="467.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_2_0">Maximo</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Price range"   datasizewidth="108.5px" datasizeheight="57.0px" dataX="26.0" dataY="418.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_1_0">RANGO DE PRECIOS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_34" class="group firer ie-background commentable non-processed" customid="Categories" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Button_4" class="button multiline manualfit firer toggle ie-background commentable non-processed" customid="Button 4"   datasizewidth="104.0px" datasizeheight="33.0px" dataX="26.0" dataY="288.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_4_0">Postres</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_3" class="button multiline manualfit firer toggle ie-background commentable non-processed" customid="Button 3"   datasizewidth="90.7px" datasizeheight="33.0px" dataX="259.3" dataY="246.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_3_0">Entradas</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_2" class="button multiline manualfit firer toggle ie-background commentable non-processed" customid="Button 2"   datasizewidth="96.7px" datasizeheight="33.0px" dataX="152.7" dataY="246.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_2_0">Bebidas</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_1" class="button multiline manualfit firer toggle ie-background commentable non-processed" customid="Button 1"   datasizewidth="114.7px" datasizeheight="33.0px" dataX="26.0" dataY="246.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_1_0">Fuertes</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_33" class="richtext autofit firer ie-background commentable non-processed" customid="Categories"   datasizewidth="117.1px" datasizeheight="19.0px" dataX="26.0" dataY="210.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_33_0">CATEGORIAS</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_35" class="group firer ie-background commentable non-processed" customid="Headline" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Line_15" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="374.0px" datasizeheight="2.0px" dataX="-0.8" dataY="100.3"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="374.5" height="1.5" viewBox="-0.75 100.25 374.5 1.5" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Line_15-97d2b" d="M0.0 101.0 L373.0 101.0 "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Line_15-97d2b" fill="none" stroke-width="0.5" stroke="#BDC0C899" stroke-linecap="square"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Reset" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_4" class="path firer commentable non-processed" customid="reset icon"   datasizewidth="18.1px" datasizeheight="17.0px" dataX="271.3" dataY="66.3"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.571491241455078" height="17.5" viewBox="271.25 66.25 18.571491241455078 17.5" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_4-97d2b" d="M288.73280029296876 71.83839988708496 C288.4586669603984 71.72853322029114 288.1482669830322 71.86186655362448 288.0405336380005 72.13599990208944 L287.7216003100077 72.93386659622192 C286.8053334554036 69.52159996032715 283.6960001627604 67.0 280.0 67.0 C275.58826649983723 67.0 272.0 70.5882667541504 272.0 75.0 C272.0 79.4117332458496 275.58826675415037 83.0 280.0 83.0 C282.1365333557129 83.0 284.1450665791829 82.16800003051758 285.65759989420576 80.65653330485026 C285.8655998865763 80.44853331247965 285.8655998865763 80.11039994557699 285.65759989420576 79.90239995320638 C285.44959990183514 79.69439996083577 285.11146653493245 79.69439996083577 284.90346654256183 79.90239995320638 C283.5925333658854 81.21226666768392 281.8506663004557 81.9333329518636 280.0 81.9333329518636 C276.1770665486654 81.9333329518636 273.06666666666666 78.82293306986492 273.06666666666666 74.99999961853027 C273.06666666666666 71.17706616719565 276.1770665486654 68.06666628519694 280.0 68.06666628519694 C283.32906672159834 68.06666628519694 286.11626688639325 70.42613283793132 286.78079986572266 73.55999972025553 L285.71093317667646 72.48906644185385 C285.50293318430585 72.28106644948323 285.16479981740315 72.28106644948323 284.95679982503253 72.48906644185385 C284.748799832662 72.69706643422444 284.748799832662 73.03519980112712 284.95679982503253 73.24319979349772 L287.0901331583659 75.37653312683105 C287.10186649163563 75.38826646010081 287.11786649227145 75.39253312647342 287.13066649039587 75.40319979389508 C287.1711998224258 75.43733312884967 287.2127998232842 75.47039979497592 287.26186649401984 75.49066646297773 C287.32586649258934 75.51946646372477 287.3962664961815 75.53333312869071 287.46666649580004 75.53333312869071 C287.5018664956093 75.53333312869071 287.53813316424686 75.5301331286629 287.57439983288447 75.52266646226246 C287.5893331666787 75.51946646223465 287.599999833107 75.5077331284682 287.61386650005977 75.50346646308898 C287.6682664990425 75.48746646344662 287.7173331697782 75.462933131059 287.7631998340289 75.43199979464214 C287.78346650004386 75.41813312768936 287.80373316605886 75.40853312810262 287.82186650037767 75.39253312746683 C287.8805331667264 75.33919979333878 287.9295998374621 75.27626646359761 287.96053316195804 75.20159978866577 C287.96053316195804 75.20053312194844 287.9615998286754 75.19946645523112 287.96266649539274 75.19839978863796 L289.0293331620594 72.53173312197129 C289.1392008463542 72.25866661071777 289.00693359375 71.94826672871908 288.73280029296876 71.83839988708496 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-97d2b" fill="#26315F" fill-opacity="0.5" stroke-width="0.5" stroke="#26315F7F" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_43" class="richtext autofit firer ie-background commentable non-processed" customid="Reset"   datasizewidth="68.5px" datasizeheight="24.0px" dataX="297.0" dataY="62.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_43_0">Reiniciar</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Hotspot_3" class="imagemap firer click ie-background commentable non-processed" customid="Reiniciar"   datasizewidth="93.5px" datasizeheight="30.0px" dataX="272.0" dataY="62.0"  >\
            <div class="clickableSpot"></div>\
          </div>\
        </div>\
\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="Previous - icon"   datasizewidth="19.0px" datasizeheight="15.1px" dataX="13.0" dataY="67.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="19.010910034179688" height="15.124763488769531" viewBox="12.99999981559806 67.00000698350527 19.010910034179688 15.124763488769531" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-97d2b" d="M16.24363642372184 73.61804039262393 L31.060605818405676 73.61804039262393 C31.58424168266349 73.61804039262393 32.01090984977775 74.04470665238955 32.01090984977775 74.56834347032168 C32.01090984977775 75.09197933457949 31.58424168266349 75.51864654801943 31.060605818405676 75.51864654801943 L16.24363642372184 75.51864654801943 L21.286060148850012 80.56107074998476 C21.615757757797766 80.96834308885195 21.57696991600089 81.56955559037783 21.169696623459387 81.89925224565127 C20.820606047287512 82.20955593370059 20.277575785294104 82.19016105912783 19.92848473228507 81.89925224565127 L13.27636363543563 75.22773773454287 C12.907878788188029 74.85925228379824 12.907878788188029 74.25804025910952 13.27636363543563 73.88955528520205 L19.92848473228507 67.23743415855029 C20.31636362709098 66.8883432396517 20.9175756517797 66.92713111870148 21.266666227951575 67.33440382264712 C21.57696991600089 67.68349475644686 21.57696991600089 68.20713109754183 21.266666227951575 68.57561595224001 L16.24363642372184 73.61804039262393 L16.24363642372184 73.61804039262393 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-97d2b" fill="#3A4B40" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Hotspot_1" class="imagemap firer click ie-background commentable non-processed" customid="Volver"   datasizewidth="25.0px" datasizeheight="24.0px" dataX="10.0" dataY="62.6"  >\
          <div class="clickableSpot"></div>\
        </div>\
        <div id="s-Paragraph_42" class="richtext autofit firer ie-background commentable non-processed" customid="Filters"   datasizewidth="67.8px" datasizeheight="25.0px" dataX="43.0" dataY="62.1" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_42_0">Filtros</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_38" class="group firer ie-background commentable non-processed" customid="Top-bar" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rect_10" class="path firer ie-background commentable non-processed" customid="Rectangle"   datasizewidth="375.0px" datasizeheight="44.0px" dataX="0.0" dataY="0.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="-1.0" height="-1.0" viewBox="0.0 0.0 -1.0 -1.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Rect_10-97d2b" d="M0.0 0.0 L375.0 0.0 L375.0 44.0 L0.0 44.0 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_10-97d2b" fill="none"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_37" class="group firer ie-background commentable non-processed" customid="Battery" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Rect_11" class="path firer ie-background commentable non-processed" customid="Border"   datasizewidth="23.0px" datasizeheight="12.3px" dataX="335.5" dataY="16.8"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="23.0" height="12.333332061767578" viewBox="335.5 16.83333396911621 23.0 12.333332061767578" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Rect_11-97d2b" d="M339.1666667461395 17.83333331346512 L354.8333332538605 17.83333331346512 C356.29622335173764 17.83333331346512 357.5 19.037109961727488 357.5 20.500000059604645 L357.5 25.499999582767487 C357.5 26.962889680644643 356.29622335173764 28.166666328907013 354.8333332538605 28.166666328907013 L339.1666667461395 28.166666328907013 C337.70377664826236 28.166666328907013 336.5 26.962889680644643 336.5 25.499999582767487 L336.5 20.500000059604645 C336.5 19.037109961727488 337.70377664826236 17.83333331346512 339.1666667461395 17.83333331346512 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_11-97d2b" fill="none" stroke-width="1.0" stroke="#000000" stroke-linecap="butt" opacity="0.35"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_46" class="path firer commentable non-processed" customid="Cap"   datasizewidth="1.3px" datasizeheight="4.0px" dataX="359.0" dataY="21.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="1.328033447265625" height="4.0" viewBox="359.0 21.0 1.328033447265625 4.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_46-97d2b" d="M359.0 21.0 L359.0 25.0 C359.80473136901855 24.66122341156006 360.3280372619629 23.873133182525635 360.3280372619629 23.0 C360.3280372619629 22.126866817474365 359.80473136901855 21.33877658843994 359.0 21.0 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_46-97d2b" fill="#000000" fill-opacity="1.0" opacity="0.4"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Rect_12" class="path firer commentable non-processed" customid="Capacity"   datasizewidth="18.0px" datasizeheight="7.3px" dataX="338.0" dataY="19.3"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.0" height="7.333332061767578" viewBox="338.0 19.33333396911621 18.0 7.333332061767578" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Rect_12-97d2b" d="M339.33333337306976 19.333333253860474 L354.66666662693024 19.333333253860474 C355.3981116758688 19.333333253860474 356.0 19.93522157799166 356.0 20.666666626930237 L356.0 25.333333373069763 C356.0 26.06477842200834 355.3981116758688 26.666666746139526 354.66666662693024 26.666666746139526 L339.33333337306976 26.666666746139526 C338.6018883241312 26.666666746139526 338.0 26.06477842200834 338.0 25.333333373069763 L338.0 20.666666626930237 C338.0 19.93522157799166 338.6018883241312 19.333333253860474 339.33333337306976 19.333333253860474 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_12-97d2b" fill="#000000" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Path_47" class="path firer commentable non-processed" customid="Wifi"   datasizewidth="15.3px" datasizeheight="11.0px" dataX="315.7" dataY="17.3"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="15.27239990234375" height="10.965570449829102" viewBox="315.6936950683594 17.330673217773438 15.27239990234375 10.965570449829102" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_47-97d2b" d="M323.330322265625 19.607999801635742 C325.5462341308594 19.608097076416016 327.6773986816406 20.459535598754883 329.2833251953125 21.9863338470459 C329.4042663574219 22.10420799255371 329.5975646972656 22.102720260620117 329.7166748046875 21.982999801635742 L330.8726806640625 20.816333770751953 C330.9329833984375 20.755611419677734 330.96661376953125 20.673358917236328 330.9660949707031 20.587779998779297 C330.9656066894531 20.502199172973633 330.9309997558594 20.420345306396484 330.8699951171875 20.360332489013672 C326.6549072265625 16.320785522460938 320.0050964355469 16.320785522460938 315.7900085449219 20.360332489013672 C315.72894287109375 20.42030143737793 315.6943054199219 20.50212860107422 315.6937255859375 20.587709426879883 C315.69317626953125 20.673290252685547 315.72674560546875 20.75556755065918 315.7869873046875 20.816333770751953 L316.9433288574219 21.982999801635742 C317.0623779296875 22.102901458740234 317.25579833984375 22.104389190673828 317.3766784667969 21.9863338470459 C318.9828186035156 20.459434509277344 321.1142272949219 19.607994079589844 323.330322265625 19.607999801635742 L323.330322265625 19.607999801635742 Z M323.330322265625 23.403667449951172 C324.5478210449219 23.403593063354492 325.72186279296875 23.856124877929688 326.62432861328125 24.6733341217041 C326.74639892578125 24.78931427001953 326.9386901855469 24.786800384521484 327.05767822265625 24.667667388916016 L328.21234130859375 23.500999450683594 C328.27313232421875 23.439804077148438 328.306884765625 23.356788635253906 328.3060302734375 23.270523071289062 C328.30511474609375 23.18425941467285 328.26971435546875 23.101945877075195 328.2076721191406 23.04199981689453 C325.45947265625 20.485618591308594 321.2035217285156 20.485618591308594 318.455322265625 23.04199981689453 C318.39324951171875 23.101945877075195 318.35784912109375 23.18429946899414 318.35699462890625 23.270591735839844 C318.356201171875 23.35688591003418 318.39007568359375 23.43989372253418 318.45098876953125 23.500999450683594 L319.6053466796875 24.667667388916016 C319.7243347167969 24.786800384521484 319.9165954589844 24.78931427001953 320.0386657714844 24.6733341217041 C320.9405517578125 23.856664657592773 322.1136474609375 23.404170989990234 323.330322265625 23.403667449951172 L323.330322265625 23.403667449951172 Z M325.5493469238281 26.187999725341797 C325.611083984375 26.12739372253418 325.6451110839844 26.043991088867188 325.6433410644531 25.95748519897461 C325.6415710449219 25.8709774017334 325.60418701171875 25.789033889770508 325.5400085449219 25.731000900268555 C324.2644348144531 24.652116775512695 322.396240234375 24.652116775512695 321.12066650390625 25.731000900268555 C321.05645751953125 25.78898811340332 321.0190124511719 25.87090492248535 321.0171813964844 25.957412719726562 C321.0153503417969 26.043920516967773 321.0492858886719 26.127347946166992 321.1109924316406 26.187999725341797 L323.1086730957031 28.20366668701172 C323.1672058105469 28.26290512084961 323.2470397949219 28.29624366760254 323.330322265625 28.29624366760254 C323.41363525390625 28.29624366760254 323.4934387207031 28.26290512084961 323.552001953125 28.20366668701172 L325.5493469238281 26.187999725341797 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_47-97d2b" fill="#000000" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_55" class="path firer commentable non-processed" customid="signal"   datasizewidth="17.0px" datasizeheight="10.7px" dataX="293.7" dataY="17.7"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="17.0" height="10.666667938232422" viewBox="293.6666564941406 17.66666603088379 17.0 10.666667938232422" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_55-97d2b" d="M294.6666564941406 24.33333396911621 L295.6666564941406 24.33333396911621 C296.2189636230469 24.33333396911621 296.6666564941406 24.781047821044922 296.6666564941406 25.33333396911621 L296.6666564941406 27.33333396911621 C296.6666564941406 27.885618209838867 296.2189636230469 28.33333396911621 295.6666564941406 28.33333396911621 L294.6666564941406 28.33333396911621 C294.1143798828125 28.33333396911621 293.6666564941406 27.885618209838867 293.6666564941406 27.33333396911621 L293.6666564941406 25.33333396911621 C293.6666564941406 24.781047821044922 294.1143798828125 24.33333396911621 294.6666564941406 24.33333396911621 L294.6666564941406 24.33333396911621 Z M299.3333435058594 22.33333396911621 L300.3333435058594 22.33333396911621 C300.8856201171875 22.33333396911621 301.3333435058594 22.781047821044922 301.3333435058594 23.33333396911621 L301.3333435058594 27.33333396911621 C301.3333435058594 27.885618209838867 300.8856201171875 28.33333396911621 300.3333435058594 28.33333396911621 L299.3333435058594 28.33333396911621 C298.7810363769531 28.33333396911621 298.3333435058594 27.885618209838867 298.3333435058594 27.33333396911621 L298.3333435058594 23.33333396911621 C298.3333435058594 22.781047821044922 298.7810363769531 22.33333396911621 299.3333435058594 22.33333396911621 Z M304.0 20.0 L305.0 20.0 C305.5522766113281 20.0 306.0 20.447715759277344 306.0 21.0 L306.0 27.33333396911621 C306.0 27.885618209838867 305.5522766113281 28.33333396911621 305.0 28.33333396911621 L304.0 28.33333396911621 C303.4477233886719 28.33333396911621 303.0 27.885618209838867 303.0 27.33333396911621 L303.0 21.0 C303.0 20.447715759277344 303.4477233886719 20.0 304.0 20.0 Z M308.6666564941406 17.66666603088379 L309.6666564941406 17.66666603088379 C310.2189636230469 17.66666603088379 310.6666564941406 18.114381790161133 310.6666564941406 18.66666603088379 L310.6666564941406 27.33333396911621 C310.6666564941406 27.885618209838867 310.2189636230469 28.33333396911621 309.6666564941406 28.33333396911621 L308.6666564941406 28.33333396911621 C308.1143798828125 28.33333396911621 307.6666564941406 27.885618209838867 307.6666564941406 27.33333396911621 L307.6666564941406 18.66666603088379 C307.6666564941406 18.114381790161133 308.1143798828125 17.66666603088379 308.6666564941406 17.66666603088379 L308.6666564941406 17.66666603088379 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_55-97d2b" fill="#000000" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_34" class="richtext autofit firer ie-background commentable non-processed" customid="Time"   datasizewidth="35.1px" datasizeheight="18.0px" dataX="28.8" dataY="15.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_34_0">10:42</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;