var content='<div class="ui-page" deviceName="iphone11pro" deviceType="mobile" deviceWidth="375" deviceHeight="812">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devMobile devIOS iphone-device canvas firer commentable non-processed" alignment="left" name="Template 1" width="375" height="812">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677126671198.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-3c7bb3f0-1584-4b54-9478-0503c8fb7b3d" class="screen growth-vertical devMobile devIOS iphone-device canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Menu" width="375" height="812">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/3c7bb3f0-1584-4b54-9478-0503c8fb7b3d-1677126671198.css" />\
      <div class="freeLayout">\
      <div id="s-Dynamic_Panel_1" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 2" datasizewidth="355.5px" datasizeheight="659.0px" dataX="19.5" dataY="57.2" >\
        <div id="s-Panel_1" class="panel default firer ie-background commentable non-processed" customid="Panel 2"  datasizewidth="355.5px" datasizeheight="659.0px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Group_19" class="group firer ie-background commentable non-processed" customid="Items" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Path_7" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="332.0px" datasizeheight="3.0px" dataX="3.5" dataY="284.8"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="332.0" height="2.0" viewBox="3.5000000000005684 284.8107604980463 332.0 2.0" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_7-3c7bb" d="M4.500000000000568 285.8107604980463 L334.50000000000057 285.8107604980463 "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-3c7bb" fill="none" stroke-width="1.0" stroke="#E3E3E3" stroke-linecap="square"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Path_8" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="332.0px" datasizeheight="3.0px" dataX="3.5" dataY="427.8"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="332.0" height="2.0" viewBox="3.5000000000005684 427.8107604980472 332.0 2.0" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_8-3c7bb" d="M4.500000000000568 428.8107604980472 L334.50000000000057 428.8107604980472 "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-3c7bb" fill="none" stroke-width="1.0" stroke="#E3E3E3" stroke-linecap="square"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Path_9" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="332.0px" datasizeheight="3.0px" dataX="3.5" dataY="572.8"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="332.0" height="2.0" viewBox="3.5000000000005684 572.8107604980466 332.0 2.0" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_9-3c7bb" d="M4.500000000000568 573.8107604980466 L334.50000000000057 573.8107604980466 "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_9-3c7bb" fill="none" stroke-width="1.0" stroke="#E3E3E3" stroke-linecap="square"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Path_10" class="path firer ie-background commentable non-processed" customid="Line"   datasizewidth="332.0px" datasizeheight="3.0px" dataX="4.5" dataY="718.8"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="332.0" height="2.0" viewBox="4.500000000000796 718.8107604980471 332.0 2.0" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_10-3c7bb" d="M5.500000000000796 719.8107604980471 L335.5000000000008 719.8107604980471 "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_10-3c7bb" fill="none" stroke-width="1.0" stroke="#E3E3E3" stroke-linecap="square"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
\
                  <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="Item 5" datasizewidth="0.0px" datasizeheight="0.0px" >\
                    <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Group 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="s-Paragraph_26" class="richtext manualfit firer ie-background commentable non-processed" customid="299-Levent\\Besiktas"   datasizewidth="168.8px" datasizeheight="51.0px" dataX="125.5" dataY="764.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_26_0">Acompa&ntilde;ada de papa al vapor, ensalada verde o encurtido de verduras.</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Paragraph_27" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="128.3px" datasizeheight="23.0px" dataX="125.5" dataY="739.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_27_0">Trucha apanada</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Paragraph" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="shapewrapper-s-Ellipse_5" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_5 non-processed"   datasizewidth="34.0px" datasizeheight="34.0px" datasizewidthpx="34.0" datasizeheightpx="34.0" dataX="301.3" dataY="809.8" >\
                          <div class="backgroundLayer">\
                            <div class="colorLayer"></div>\
                            <div class="imageLayer"></div>\
                          </div>\
                          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_5" class="svgContainer" style="width:100%; height:100%;">\
                              <g>\
                                  <g clip-path="url(#clip-s-Ellipse_5)">\
                                          <ellipse id="s-Ellipse_5" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </g>\
                              </g>\
                              <defs>\
                                  <clipPath id="clip-s-Ellipse_5" class="clipPath">\
                                          <ellipse cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </clipPath>\
                              </defs>\
                          </svg>\
                          <div class="paddingLayer">\
                              <div id="shapert-s-Ellipse_5" class="content firer" >\
                                  <div class="valign">\
                                      <span id="rtr-s-Ellipse_5_0"></span>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      <div id="s-Path_11" class="path firer commentable non-processed" customid="plus"   datasizewidth="11.0px" datasizeheight="11.0px" dataX="312.3" dataY="820.8"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="11.0" height="11.0" viewBox="312.2973632812502 820.8107604980463 11.0 11.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="s-Path_11-3c7bb" d="M323.0582328464676 827.0281518023942 C323.2017111130385 827.0281518023942 323.2973632812502 826.9324996270558 323.2973632812502 826.7890213676116 L323.2973632812502 825.8324996284811 C323.2973632812502 825.6890213619102 323.20171110591184 825.5933691936984 323.0582328464676 825.5933691936984 L318.9930154551633 825.5933691936984 C318.7060589220215 825.5933691936984 318.51475458559804 825.4020648430218 318.51475458559804 825.1151083241333 L318.51475458559804 821.0498909328289 C318.51475458559804 820.906412666258 318.41910241025965 820.8107604980463 318.2756241508154 820.8107604980463 L317.31910241168504 820.8107604980463 C317.1756241451141 820.8107604980463 317.0799719769024 820.9064126733847 317.0799719769024 821.0498909328289 L317.0799719769024 825.1151083241333 C317.0799719769024 825.402064857275 316.88866762622564 825.5933691936984 316.60171110733717 825.5933691936984 L312.53649371603285 825.5933691936984 C312.393015449462 825.5933691936984 312.2973632812502 825.6890213690368 312.2973632812502 825.8324996284811 L312.2973632812502 826.7890213676116 C312.2973632812502 826.9324996341824 312.3930154565886 827.0281518023942 312.53649371603285 827.0281518023942 L316.60171110733717 827.0281518023942 C316.88866764047896 827.0281518023942 317.0799719769024 827.2194561530708 317.0799719769024 827.5064126719593 L317.0799719769024 831.5716300632637 C317.0799719769024 831.7151083298346 317.1756241522408 831.8107604980463 317.31910241168504 831.8107604980463 L318.2756241508154 831.8107604980463 C318.41910241738634 831.8107604980463 318.51475458559804 831.7151083227079 318.51475458559804 831.5716300632637 L318.51475458559804 827.5064126719593 C318.51475458559804 827.2194561388176 318.7060589362748 827.0281518023942 318.9930154551633 827.0281518023942 L323.0582328464676 827.0281518023942 Z "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_11-3c7bb" fill="#FFFFFF" fill-opacity="1.0"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Hotspot_11" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 8"   datasizewidth="36.0px" datasizeheight="34.0px" dataX="300.3" dataY="809.8"  >\
                        <div class="clickableSpot"></div>\
                      </div>\
                    </div>\
\
                    <div id="s-Paragraph_28" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="92.2px" datasizeheight="23.0px" dataX="127.5" dataY="820.8" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_28_0">18.000 COP</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Image_12" class="image lockV firer ie-background commentable non-processed" customid="Image"   datasizewidth="100.0px" datasizeheight="100.0px" dataX="4.5" dataY="744.0" aspectRatio="1.0"   alt="image">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                      		<img src="./images/2140c36e-22ee-4c18-83ff-69a7cc6506ad.png" />\
                      	</div>\
                      </div>\
                    </div>\
\
                  </div>\
\
\
                  <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Item 4" datasizewidth="0.0px" datasizeheight="0.0px" >\
                    <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Group 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="s-Paragraph_23" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="168.8px" datasizeheight="51.0px" dataX="125.5" dataY="618.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_23_0">En salsa a la BBQ secreto de la casa.</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Paragraph_24" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="119.8px" datasizeheight="23.0px" dataX="125.5" dataY="593.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_24_0">Porci&oacute;n costilla</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="Paragraph" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="shapewrapper-s-Ellipse_4" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_4 non-processed"   datasizewidth="34.0px" datasizeheight="34.0px" datasizewidthpx="34.0" datasizeheightpx="34.0" dataX="301.3" dataY="663.8" >\
                          <div class="backgroundLayer">\
                            <div class="colorLayer"></div>\
                            <div class="imageLayer"></div>\
                          </div>\
                          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_4" class="svgContainer" style="width:100%; height:100%;">\
                              <g>\
                                  <g clip-path="url(#clip-s-Ellipse_4)">\
                                          <ellipse id="s-Ellipse_4" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </g>\
                              </g>\
                              <defs>\
                                  <clipPath id="clip-s-Ellipse_4" class="clipPath">\
                                          <ellipse cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </clipPath>\
                              </defs>\
                          </svg>\
                          <div class="paddingLayer">\
                              <div id="shapert-s-Ellipse_4" class="content firer" >\
                                  <div class="valign">\
                                      <span id="rtr-s-Ellipse_4_0"></span>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      <div id="s-Path_6" class="path firer commentable non-processed" customid="plus"   datasizewidth="11.0px" datasizeheight="11.0px" dataX="312.3" dataY="674.8"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="11.0" height="11.0" viewBox="312.2973632812502 674.810760498046 11.0 11.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="s-Path_6-3c7bb" d="M323.0582328464676 681.0281518023938 C323.2017111130385 681.0281518023938 323.2973632812502 680.9324996270554 323.2973632812502 680.7890213676112 L323.2973632812502 679.8324996284807 C323.2973632812502 679.6890213619099 323.20171110591184 679.5933691936981 323.0582328464676 679.5933691936981 L318.9930154551633 679.5933691936981 C318.7060589220215 679.5933691936981 318.51475458559804 679.4020648430214 318.51475458559804 679.115108324133 L318.51475458559804 675.0498909328286 C318.51475458559804 674.9064126662577 318.41910241025965 674.810760498046 318.2756241508154 674.810760498046 L317.31910241168504 674.810760498046 C317.1756241451141 674.810760498046 317.0799719769024 674.9064126733844 317.0799719769024 675.0498909328286 L317.0799719769024 679.115108324133 C317.0799719769024 679.4020648572747 316.88866762622564 679.5933691936981 316.60171110733717 679.5933691936981 L312.53649371603285 679.5933691936981 C312.393015449462 679.5933691936981 312.2973632812502 679.6890213690365 312.2973632812502 679.8324996284807 L312.2973632812502 680.7890213676112 C312.2973632812502 680.932499634182 312.3930154565886 681.0281518023938 312.53649371603285 681.0281518023938 L316.60171110733717 681.0281518023938 C316.88866764047896 681.0281518023938 317.0799719769024 681.2194561530705 317.0799719769024 681.506412671959 L317.0799719769024 685.5716300632633 C317.0799719769024 685.7151083298343 317.1756241522408 685.810760498046 317.31910241168504 685.810760498046 L318.2756241508154 685.810760498046 C318.41910241738634 685.810760498046 318.51475458559804 685.7151083227076 318.51475458559804 685.5716300632633 L318.51475458559804 681.506412671959 C318.51475458559804 681.2194561388172 318.7060589362748 681.0281518023938 318.9930154551633 681.0281518023938 L323.0582328464676 681.0281518023938 Z "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-3c7bb" fill="#FFFFFF" fill-opacity="1.0"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Hotspot_10" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 8"   datasizewidth="34.0px" datasizeheight="34.0px" dataX="301.3" dataY="663.8"  >\
                        <div class="clickableSpot"></div>\
                      </div>\
                    </div>\
\
                    <div id="s-Paragraph_25" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="92.2px" datasizeheight="23.0px" dataX="127.5" dataY="674.8" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_25_0">15.000 COP</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Image_11" class="image lockV firer ie-background commentable non-processed" customid="Image"   datasizewidth="100.0px" datasizeheight="100.0px" dataX="4.5" dataY="597.8" aspectRatio="1.0"   alt="image">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                      		<img src="./images/e3898954-978a-4c34-9e51-6c62c8425b9f.png" />\
                      	</div>\
                      </div>\
                    </div>\
\
                  </div>\
\
\
                  <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="Item 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
                    <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Group 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="s-Paragraph_20" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="168.8px" datasizeheight="51.0px" dataX="125.5" dataY="471.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_20_0">Acompa&ntilde;ada de papa a la francesa.</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Paragraph_21" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="113.7px" datasizeheight="23.0px" dataX="125.5" dataY="446.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_21_0">Alitas picantes</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Paragraph" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="shapewrapper-s-Ellipse_3" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_3 non-processed"   datasizewidth="34.0px" datasizeheight="34.0px" datasizewidthpx="34.0" datasizeheightpx="34.0" dataX="301.3" dataY="516.8" >\
                          <div class="backgroundLayer">\
                            <div class="colorLayer"></div>\
                            <div class="imageLayer"></div>\
                          </div>\
                          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_3" class="svgContainer" style="width:100%; height:100%;">\
                              <g>\
                                  <g clip-path="url(#clip-s-Ellipse_3)">\
                                          <ellipse id="s-Ellipse_3" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </g>\
                              </g>\
                              <defs>\
                                  <clipPath id="clip-s-Ellipse_3" class="clipPath">\
                                          <ellipse cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </clipPath>\
                              </defs>\
                          </svg>\
                          <div class="paddingLayer">\
                              <div id="shapert-s-Ellipse_3" class="content firer" >\
                                  <div class="valign">\
                                      <span id="rtr-s-Ellipse_3_0"></span>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      <div id="s-Path_5" class="path firer commentable non-processed" customid="plus"   datasizewidth="11.0px" datasizeheight="11.0px" dataX="312.3" dataY="527.8"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="11.0" height="11.0" viewBox="312.2973632812502 527.8107604980461 11.0 11.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="s-Path_5-3c7bb" d="M323.0582328464676 534.028151802394 C323.2017111130385 534.028151802394 323.2973632812502 533.9324996270556 323.2973632812502 533.7890213676113 L323.2973632812502 532.8324996284808 C323.2973632812502 532.68902136191 323.20171110591184 532.5933691936982 323.0582328464676 532.5933691936982 L318.9930154551633 532.5933691936982 C318.7060589220215 532.5933691936982 318.51475458559804 532.4020648430215 318.51475458559804 532.1151083241331 L318.51475458559804 528.0498909328287 C318.51475458559804 527.9064126662578 318.41910241025965 527.8107604980461 318.2756241508154 527.8107604980461 L317.31910241168504 527.8107604980461 C317.1756241451141 527.8107604980461 317.0799719769024 527.9064126733845 317.0799719769024 528.0498909328287 L317.0799719769024 532.1151083241331 C317.0799719769024 532.4020648572748 316.88866762622564 532.5933691936982 316.60171110733717 532.5933691936982 L312.53649371603285 532.5933691936982 C312.393015449462 532.5933691936982 312.2973632812502 532.6890213690366 312.2973632812502 532.8324996284808 L312.2973632812502 533.7890213676113 C312.2973632812502 533.9324996341821 312.3930154565886 534.028151802394 312.53649371603285 534.028151802394 L316.60171110733717 534.028151802394 C316.88866764047896 534.028151802394 317.0799719769024 534.2194561530706 317.0799719769024 534.5064126719591 L317.0799719769024 538.5716300632635 C317.0799719769024 538.7151083298344 317.1756241522408 538.8107604980461 317.31910241168504 538.8107604980461 L318.2756241508154 538.8107604980461 C318.41910241738634 538.8107604980461 318.51475458559804 538.7151083227077 318.51475458559804 538.5716300632635 L318.51475458559804 534.5064126719591 C318.51475458559804 534.2194561388173 318.7060589362748 534.028151802394 318.9930154551633 534.028151802394 L323.0582328464676 534.028151802394 Z "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-3c7bb" fill="#FFFFFF" fill-opacity="1.0"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Hotspot_9" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 8"   datasizewidth="34.0px" datasizeheight="34.0px" dataX="301.3" dataY="516.3"  >\
                        <div class="clickableSpot"></div>\
                      </div>\
                    </div>\
\
                    <div id="s-Paragraph_22" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="92.2px" datasizeheight="23.0px" dataX="127.5" dataY="527.8" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_22_0">16.000 COP</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Image_10" class="image lockV firer ie-background commentable non-processed" customid="Image"   datasizewidth="100.0px" datasizeheight="100.0px" dataX="4.5" dataY="451.0" aspectRatio="1.0"   alt="image">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                      		<img src="./images/dbbcdf5f-a6d8-45c4-bc36-4fc42dd45653.png" />\
                      	</div>\
                      </div>\
                    </div>\
\
                  </div>\
\
\
                  <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Item 2" datasizewidth="0.0px" datasizeheight="0.0px" >\
                    <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Group 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="s-Paragraph_17" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="168.8px" datasizeheight="51.0px" dataX="125.5" dataY="328.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_17_0">Acompa&ntilde;ada de papa al vapor, ensalada verde o encurtido de verduras.</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Paragraph_18" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="88.0px" datasizeheight="23.0px" dataX="125.5" dataY="303.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_18_0">Chicharr&oacute;n</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Paragraph" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="shapewrapper-s-Ellipse_2" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="34.0px" datasizeheight="34.0px" datasizewidthpx="34.0" datasizeheightpx="34.0" dataX="301.3" dataY="373.8" >\
                          <div class="backgroundLayer">\
                            <div class="colorLayer"></div>\
                            <div class="imageLayer"></div>\
                          </div>\
                          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
                              <g>\
                                  <g clip-path="url(#clip-s-Ellipse_2)">\
                                          <ellipse id="s-Ellipse_2" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </g>\
                              </g>\
                              <defs>\
                                  <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                                          <ellipse cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </clipPath>\
                              </defs>\
                          </svg>\
                          <div class="paddingLayer">\
                              <div id="shapert-s-Ellipse_2" class="content firer" >\
                                  <div class="valign">\
                                      <span id="rtr-s-Ellipse_2_0"></span>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      <div id="s-Path_4" class="path firer commentable non-processed" customid="plus"   datasizewidth="11.0px" datasizeheight="11.0px" dataX="312.3" dataY="384.8"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="11.0" height="11.0" viewBox="312.2973632812502 384.8107604980469 11.0 11.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="s-Path_4-3c7bb" d="M323.0582328464676 391.0281518023947 C323.2017111130385 391.0281518023947 323.2973632812502 390.9324996270563 323.2973632812502 390.78902136761207 L323.2973632812502 389.8324996284817 C323.2973632812502 389.68902136191076 323.20171110591184 389.59336919369906 323.0582328464676 389.59336919369906 L318.9930154551633 389.59336919369906 C318.7060589220215 389.59336919369906 318.51475458559804 389.4020648430223 318.51475458559804 389.1151083241338 L318.51475458559804 385.0498909328295 C318.51475458559804 384.90641266625863 318.41910241025965 384.8107604980469 318.2756241508154 384.8107604980469 L317.31910241168504 384.8107604980469 C317.1756241451141 384.8107604980469 317.0799719769024 384.90641267338526 317.0799719769024 385.0498909328295 L317.0799719769024 389.1151083241338 C317.0799719769024 389.4020648572756 316.88866762622564 389.59336919369906 316.60171110733717 389.59336919369906 L312.53649371603285 389.59336919369906 C312.393015449462 389.59336919369906 312.2973632812502 389.68902136903745 312.2973632812502 389.8324996284817 L312.2973632812502 390.78902136761207 C312.2973632812502 390.932499634183 312.3930154565886 391.0281518023947 312.53649371603285 391.0281518023947 L316.60171110733717 391.0281518023947 C316.88866764047896 391.0281518023947 317.0799719769024 391.21945615307146 317.0799719769024 391.50641267195994 L317.0799719769024 395.57163006326425 C317.0799719769024 395.7151083298352 317.1756241522408 395.8107604980469 317.31910241168504 395.8107604980469 L318.2756241508154 395.8107604980469 C318.41910241738634 395.8107604980469 318.51475458559804 395.7151083227085 318.51475458559804 395.57163006326425 L318.51475458559804 391.50641267195994 C318.51475458559804 391.21945613881815 318.7060589362748 391.0281518023947 318.9930154551633 391.0281518023947 L323.0582328464676 391.0281518023947 Z "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-3c7bb" fill="#FFFFFF" fill-opacity="1.0"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Hotspot_8" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 8"   datasizewidth="34.0px" datasizeheight="34.0px" dataX="301.3" dataY="373.8"  >\
                        <div class="clickableSpot"></div>\
                      </div>\
                    </div>\
\
                    <div id="s-Paragraph_19" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="92.2px" datasizeheight="23.0px" dataX="127.5" dataY="384.8" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_19_0">13.000 COP</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Image_9" class="image lockV firer ie-background commentable non-processed" customid="Image"   datasizewidth="100.0px" datasizeheight="100.0px" dataX="4.5" dataY="307.0" aspectRatio="1.0"   alt="image">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                      		<img src="./images/4c3f59f4-39b0-4887-8877-3a3882176025.png" />\
                      	</div>\
                      </div>\
                    </div>\
\
                  </div>\
\
\
                  <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="Item 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                    <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Group 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="s-Paragraph_16" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="168.8px" datasizeheight="51.0px" dataX="125.5" dataY="185.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_16_0">Acompa&ntilde;ado de papa al vapor, ensalada verde o encurtido de verduras.</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Paragraph_14" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="120.3px" datasizeheight="23.0px" dataX="125.5" dataY="160.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-s-Paragraph_14_0">Chorizo Casero</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
\
                    <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="Paragraph" datasizewidth="0.0px" datasizeheight="0.0px" >\
                      <div id="shapewrapper-s-Ellipse_1" customid="Ellipse 1" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="34.0px" datasizeheight="34.0px" datasizewidthpx="34.0" datasizeheightpx="34.0" dataX="301.3" dataY="230.8" >\
                          <div class="backgroundLayer">\
                            <div class="colorLayer"></div>\
                            <div class="imageLayer"></div>\
                          </div>\
                          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                              <g>\
                                  <g clip-path="url(#clip-s-Ellipse_1)">\
                                          <ellipse id="s-Ellipse_1" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 1" cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </g>\
                              </g>\
                              <defs>\
                                  <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                                          <ellipse cx="17.0" cy="17.0" rx="17.0" ry="17.0">\
                                          </ellipse>\
                                  </clipPath>\
                              </defs>\
                          </svg>\
                          <div class="paddingLayer">\
                              <div id="shapert-s-Ellipse_1" class="content firer" >\
                                  <div class="valign">\
                                      <span id="rtr-s-Ellipse_1_0"></span>\
                                  </div>\
                              </div>\
                          </div>\
                      </div>\
                      <div id="s-Path_3" class="path firer commentable non-processed" customid="plus"   datasizewidth="11.0px" datasizeheight="11.0px" dataX="312.3" dataY="241.8"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="11.0" height="11.0" viewBox="312.2973632812502 241.81076049804628 11.0 11.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="s-Path_3-3c7bb" d="M323.0582328464676 248.0281518023941 C323.2017111130385 248.0281518023941 323.2973632812502 247.93249962705573 323.2973632812502 247.7890213676115 L323.2973632812502 246.83249962848106 C323.2973632812502 246.6890213619102 323.20171110591184 246.59336919369846 323.0582328464676 246.59336919369846 L318.9930154551633 246.59336919369846 C318.7060589220215 246.59336919369846 318.51475458559804 246.40206484302172 318.51475458559804 246.11510832413325 L318.51475458559804 242.04989093282887 C318.51475458559804 241.906412666258 318.41910241025965 241.81076049804628 318.2756241508154 241.81076049804628 L317.31910241168504 241.81076049804628 C317.1756241451141 241.81076049804628 317.0799719769024 241.90641267338464 317.0799719769024 242.04989093282887 L317.0799719769024 246.11510832413325 C317.0799719769024 246.40206485727498 316.88866762622564 246.59336919369846 316.60171110733717 246.59336919369846 L312.53649371603285 246.59336919369846 C312.393015449462 246.59336919369846 312.2973632812502 246.68902136903682 312.2973632812502 246.83249962848106 L312.2973632812502 247.7890213676115 C312.2973632812502 247.93249963418236 312.3930154565886 248.0281518023941 312.53649371603285 248.0281518023941 L316.60171110733717 248.0281518023941 C316.88866764047896 248.0281518023941 317.0799719769024 248.21945615307084 317.0799719769024 248.5064126719593 L317.0799719769024 252.57163006326368 C317.0799719769024 252.71510832983455 317.1756241522408 252.81076049804628 317.31910241168504 252.81076049804628 L318.2756241508154 252.81076049804628 C318.41910241738634 252.81076049804628 318.51475458559804 252.71510832270792 318.51475458559804 252.57163006326368 L318.51475458559804 248.5064126719593 C318.51475458559804 248.21945613881758 318.7060589362748 248.0281518023941 318.9930154551633 248.0281518023941 L323.0582328464676 248.0281518023941 Z "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-3c7bb" fill="#FFFFFF" fill-opacity="1.0"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
                      <div id="s-Hotspot_7" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 7"   datasizewidth="34.0px" datasizeheight="34.0px" dataX="301.3" dataY="230.8"  >\
                        <div class="clickableSpot"></div>\
                      </div>\
                    </div>\
\
                    <div id="s-Paragraph_15" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="92.2px" datasizeheight="23.0px" dataX="127.5" dataY="241.8" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Paragraph_15_0">13.000 COP</span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Image_8" class="image lockV firer ie-background commentable non-processed" customid="Image"   datasizewidth="100.0px" datasizeheight="100.0px" dataX="4.5" dataY="162.0" aspectRatio="1.0"   alt="image">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                      		<img src="./images/1a5c71d1-e7e1-4895-bcbf-290b286136b0.png" />\
                      	</div>\
                      </div>\
                    </div>\
\
                  </div>\
\
                </div>\
\
                <div id="s-Dynamic_Panel_2" class="dynamicpanel firer ie-background commentable non-processed" customid="Dynamic Panel 1" datasizewidth="332.5px" datasizeheight="44.0px" dataX="6.5" dataY="89.3" >\
                  <div id="s-Panel_2" class="panel default firer ie-background commentable non-processed" customid="Panel 1"  datasizewidth="332.5px" datasizeheight="44.0px" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                    	<div class="layoutWrapper scrollable">\
                    	  <div class="paddingLayer">\
                          <div class="freeLayout">\
                          <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Categories" datasizewidth="0.0px" datasizeheight="0.0px" >\
                            <div id="s-Group_30" class="group firer ie-background commentable non-processed" customid="Card 4 - Category" datasizewidth="0.0px" datasizeheight="0.0px" >\
                              <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 1"   datasizewidth="88.0px" datasizeheight="35.0px" datasizewidthpx="87.99999999999977" datasizeheightpx="35.0" dataX="329.0" dataY="-0.0" >\
                                <div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div>\
                                <div class="borderLayer">\
                                  <div class="paddingLayer">\
                                    <div class="content">\
                                      <div class="valign">\
                                        <span id="rtr-s-Rectangle_4_0">Postres</span>\
                                      </div>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                              <div id="s-Hotspot_4" class="imagemap firer toggle ie-background commentable non-processed" customid="Hotspot 4"   datasizewidth="88.0px" datasizeheight="35.0px" dataX="329.0" dataY="-0.0"  >\
                                <div class="clickableSpot"></div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Group_29" class="group firer ie-background commentable non-processed" customid="Card 3 - Category" datasizewidth="0.0px" datasizeheight="0.0px" >\
                              <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 1"   datasizewidth="88.0px" datasizeheight="35.0px" datasizewidthpx="88.0" datasizeheightpx="35.0" dataX="227.1" dataY="-0.0" >\
                                <div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div>\
                                <div class="borderLayer">\
                                  <div class="paddingLayer">\
                                    <div class="content">\
                                      <div class="valign">\
                                        <span id="rtr-s-Rectangle_3_0">Bebidas</span>\
                                      </div>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                              <div id="s-Hotspot_3" class="imagemap firer toggle ie-background commentable non-processed" customid="Hotspot 3"   datasizewidth="88.0px" datasizeheight="35.0px" dataX="227.1" dataY="-0.0"  >\
                                <div class="clickableSpot"></div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Group_28" class="group firer ie-background commentable non-processed" customid="Card 2 - Category" datasizewidth="0.0px" datasizeheight="0.0px" >\
                              <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 1"   datasizewidth="88.0px" datasizeheight="35.0px" datasizewidthpx="88.0" datasizeheightpx="35.0" dataX="122.0" dataY="-0.0" >\
                                <div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div>\
                                <div class="borderLayer">\
                                  <div class="paddingLayer">\
                                    <div class="content">\
                                      <div class="valign">\
                                        <span id="rtr-s-Rectangle_2_0">Fuertes</span>\
                                      </div>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                              <div id="s-Hotspot_2" class="imagemap firer toggle ie-background commentable non-processed" customid="Hotspot 2"   datasizewidth="88.0px" datasizeheight="35.0px" dataX="122.0" dataY="-0.0"  >\
                                <div class="clickableSpot"></div>\
                              </div>\
                            </div>\
\
\
                            <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Card 1 - Category" datasizewidth="0.0px" datasizeheight="0.0px" >\
                              <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Rectangle"   datasizewidth="90.0px" datasizeheight="37.0px" datasizewidthpx="90.0" datasizeheightpx="37.0" dataX="17.0" dataY="-0.0" >\
                                <div class="backgroundLayer">\
                                  <div class="colorLayer"></div>\
                                  <div class="imageLayer"></div>\
                                </div>\
                                <div class="borderLayer">\
                                  <div class="paddingLayer">\
                                    <div class="content">\
                                      <div class="valign">\
                                        <span id="rtr-s-Rectangle_1_0">Entradas</span>\
                                      </div>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                              <div id="s-Hotspot_1" class="imagemap firer toggle ie-background commentable non-processed" customid="Hotspot 1"   datasizewidth="90.0px" datasizeheight="37.0px" dataX="17.0" dataY="-0.0"  >\
                                <div class="clickableSpot"></div>\
                              </div>\
                            </div>\
\
                          </div>\
\
                          </div>\
\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Headline" datasizewidth="0.0px" datasizeheight="0.0px" >\
                  <div id="s-Paragraph_13" class="richtext manualfit firer ie-background commentable non-processed" customid="H1"   datasizewidth="138.4px" datasizeheight="35.0px" dataX="6.5" dataY="41.3" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_13_0">Menu</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Path_38" class="path firer commentable non-processed" customid="Previous icon"   datasizewidth="19.0px" datasizeheight="15.1px" dataX="5.7" dataY="10.0"  >\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg xmlns="http://www.w3.org/2000/svg" width="19.010910034179688" height="15.124763488769531" viewBox="5.68909072876064 9.999999999999673 19.010910034179688 15.124763488769531" preserveAspectRatio="none">\
                      	  <g>\
                      	    <defs>\
                      	      <path id="s-Path_38-3c7bb" d="M8.932727336884419 16.618033409118325 L23.749696731568257 16.618033409118325 C24.27333259582607 16.618033409118325 24.700000762940327 17.04469966888395 24.700000762940327 17.56833648681608 C24.700000762940327 18.091972351073892 24.27333259582607 18.518639564513833 23.749696731568257 18.518639564513833 L8.932727336884419 18.518639564513833 L13.975151062012593 23.561063766479165 C14.304848670960347 23.968336105346353 14.266060829163472 24.56954860687223 13.858787536621968 24.89924526214567 C13.509696960450093 25.209548950194986 12.966666698456685 25.19015407562223 12.617575645447651 24.89924526214567 L5.96545454859821 18.22773075103727 C5.596969701350609 17.859245300292642 5.596969701350609 17.25803327560392 5.96545454859821 16.88954830169645 L12.617575645447651 10.237427175044687 C13.00545454025356 9.888336256146104 13.60666656494228 9.927124135195882 13.955757141114155 10.334396839141519 C14.266060829163472 10.683487772941263 14.266060829163472 11.207124114036233 13.955757141114155 11.575608968734414 L8.932727336884419 16.618033409118325 L8.932727336884419 16.618033409118325 Z "></path>\
                      	    </defs>\
                      	    <g style="mix-blend-mode:normal">\
                      	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_38-3c7bb" fill="#3A4B40" fill-opacity="1.0"></use>\
                      	    </g>\
                      	  </g>\
                      	</svg>\
\
                      </div>\
                    </div>\
                  </div>\
                  <div id="s-Hotspot_5" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 5"   datasizewidth="28.0px" datasizeheight="24.0px" dataX="1.2" dataY="5.6"  >\
                    <div class="clickableSpot"></div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_38" class="group firer ie-background commentable non-processed" customid="Top-bar" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rect_10" class="path firer ie-background commentable non-processed" customid="Rectangle"   datasizewidth="375.0px" datasizeheight="44.0px" dataX="0.0" dataY="0.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="-1.0" height="-1.0" viewBox="0.0 0.0 -1.0 -1.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Rect_10-3c7bb" d="M0.0 0.0 L375.0 0.0 L375.0 44.0 L0.0 44.0 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_10-3c7bb" fill="none"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_37" class="group firer ie-background commentable non-processed" customid="Battery" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Rect_11" class="path firer ie-background commentable non-processed" customid="Border"   datasizewidth="23.0px" datasizeheight="12.3px" dataX="335.5" dataY="16.8"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="23.0" height="12.333332061767578" viewBox="335.5 16.83333396911621 23.0 12.333332061767578" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Rect_11-3c7bb" d="M339.1666667461395 17.83333331346512 L354.8333332538605 17.83333331346512 C356.29622335173764 17.83333331346512 357.5 19.037109961727488 357.5 20.500000059604645 L357.5 25.499999582767487 C357.5 26.962889680644643 356.29622335173764 28.166666328907013 354.8333332538605 28.166666328907013 L339.1666667461395 28.166666328907013 C337.70377664826236 28.166666328907013 336.5 26.962889680644643 336.5 25.499999582767487 L336.5 20.500000059604645 C336.5 19.037109961727488 337.70377664826236 17.83333331346512 339.1666667461395 17.83333331346512 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_11-3c7bb" fill="none" stroke-width="1.0" stroke="#000000" stroke-linecap="butt" opacity="0.35"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_46" class="path firer commentable non-processed" customid="Cap"   datasizewidth="1.3px" datasizeheight="4.0px" dataX="359.0" dataY="21.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="1.328033447265625" height="4.0" viewBox="359.0 21.0 1.328033447265625 4.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_46-3c7bb" d="M359.0 21.0 L359.0 25.0 C359.80473136901855 24.66122341156006 360.3280372619629 23.873133182525635 360.3280372619629 23.0 C360.3280372619629 22.126866817474365 359.80473136901855 21.33877658843994 359.0 21.0 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_46-3c7bb" fill="#000000" fill-opacity="1.0" opacity="0.4"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Rect_12" class="path firer commentable non-processed" customid="Capacity"   datasizewidth="18.0px" datasizeheight="7.3px" dataX="338.0" dataY="19.3"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.0" height="7.333332061767578" viewBox="338.0 19.33333396911621 18.0 7.333332061767578" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Rect_12-3c7bb" d="M339.33333337306976 19.333333253860474 L354.66666662693024 19.333333253860474 C355.3981116758688 19.333333253860474 356.0 19.93522157799166 356.0 20.666666626930237 L356.0 25.333333373069763 C356.0 26.06477842200834 355.3981116758688 26.666666746139526 354.66666662693024 26.666666746139526 L339.33333337306976 26.666666746139526 C338.6018883241312 26.666666746139526 338.0 26.06477842200834 338.0 25.333333373069763 L338.0 20.666666626930237 C338.0 19.93522157799166 338.6018883241312 19.333333253860474 339.33333337306976 19.333333253860474 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Rect_12-3c7bb" fill="#000000" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Path_47" class="path firer commentable non-processed" customid="Wifi"   datasizewidth="15.3px" datasizeheight="11.0px" dataX="315.7" dataY="17.3"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="15.27239990234375" height="10.965570449829102" viewBox="315.6936950683594 17.330673217773438 15.27239990234375 10.965570449829102" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_47-3c7bb" d="M323.330322265625 19.607999801635742 C325.5462341308594 19.608097076416016 327.6773986816406 20.459535598754883 329.2833251953125 21.9863338470459 C329.4042663574219 22.10420799255371 329.5975646972656 22.102720260620117 329.7166748046875 21.982999801635742 L330.8726806640625 20.816333770751953 C330.9329833984375 20.755611419677734 330.96661376953125 20.673358917236328 330.9660949707031 20.587779998779297 C330.9656066894531 20.502199172973633 330.9309997558594 20.420345306396484 330.8699951171875 20.360332489013672 C326.6549072265625 16.320785522460938 320.0050964355469 16.320785522460938 315.7900085449219 20.360332489013672 C315.72894287109375 20.42030143737793 315.6943054199219 20.50212860107422 315.6937255859375 20.587709426879883 C315.69317626953125 20.673290252685547 315.72674560546875 20.75556755065918 315.7869873046875 20.816333770751953 L316.9433288574219 21.982999801635742 C317.0623779296875 22.102901458740234 317.25579833984375 22.104389190673828 317.3766784667969 21.9863338470459 C318.9828186035156 20.459434509277344 321.1142272949219 19.607994079589844 323.330322265625 19.607999801635742 L323.330322265625 19.607999801635742 Z M323.330322265625 23.403667449951172 C324.5478210449219 23.403593063354492 325.72186279296875 23.856124877929688 326.62432861328125 24.6733341217041 C326.74639892578125 24.78931427001953 326.9386901855469 24.786800384521484 327.05767822265625 24.667667388916016 L328.21234130859375 23.500999450683594 C328.27313232421875 23.439804077148438 328.306884765625 23.356788635253906 328.3060302734375 23.270523071289062 C328.30511474609375 23.18425941467285 328.26971435546875 23.101945877075195 328.2076721191406 23.04199981689453 C325.45947265625 20.485618591308594 321.2035217285156 20.485618591308594 318.455322265625 23.04199981689453 C318.39324951171875 23.101945877075195 318.35784912109375 23.18429946899414 318.35699462890625 23.270591735839844 C318.356201171875 23.35688591003418 318.39007568359375 23.43989372253418 318.45098876953125 23.500999450683594 L319.6053466796875 24.667667388916016 C319.7243347167969 24.786800384521484 319.9165954589844 24.78931427001953 320.0386657714844 24.6733341217041 C320.9405517578125 23.856664657592773 322.1136474609375 23.404170989990234 323.330322265625 23.403667449951172 L323.330322265625 23.403667449951172 Z M325.5493469238281 26.187999725341797 C325.611083984375 26.12739372253418 325.6451110839844 26.043991088867188 325.6433410644531 25.95748519897461 C325.6415710449219 25.8709774017334 325.60418701171875 25.789033889770508 325.5400085449219 25.731000900268555 C324.2644348144531 24.652116775512695 322.396240234375 24.652116775512695 321.12066650390625 25.731000900268555 C321.05645751953125 25.78898811340332 321.0190124511719 25.87090492248535 321.0171813964844 25.957412719726562 C321.0153503417969 26.043920516967773 321.0492858886719 26.127347946166992 321.1109924316406 26.187999725341797 L323.1086730957031 28.20366668701172 C323.1672058105469 28.26290512084961 323.2470397949219 28.29624366760254 323.330322265625 28.29624366760254 C323.41363525390625 28.29624366760254 323.4934387207031 28.26290512084961 323.552001953125 28.20366668701172 L325.5493469238281 26.187999725341797 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_47-3c7bb" fill="#000000" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_55" class="path firer commentable non-processed" customid="signal"   datasizewidth="17.0px" datasizeheight="10.7px" dataX="293.7" dataY="17.7"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="17.0" height="10.666667938232422" viewBox="293.6666564941406 17.66666603088379 17.0 10.666667938232422" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_55-3c7bb" d="M294.6666564941406 24.33333396911621 L295.6666564941406 24.33333396911621 C296.2189636230469 24.33333396911621 296.6666564941406 24.781047821044922 296.6666564941406 25.33333396911621 L296.6666564941406 27.33333396911621 C296.6666564941406 27.885618209838867 296.2189636230469 28.33333396911621 295.6666564941406 28.33333396911621 L294.6666564941406 28.33333396911621 C294.1143798828125 28.33333396911621 293.6666564941406 27.885618209838867 293.6666564941406 27.33333396911621 L293.6666564941406 25.33333396911621 C293.6666564941406 24.781047821044922 294.1143798828125 24.33333396911621 294.6666564941406 24.33333396911621 L294.6666564941406 24.33333396911621 Z M299.3333435058594 22.33333396911621 L300.3333435058594 22.33333396911621 C300.8856201171875 22.33333396911621 301.3333435058594 22.781047821044922 301.3333435058594 23.33333396911621 L301.3333435058594 27.33333396911621 C301.3333435058594 27.885618209838867 300.8856201171875 28.33333396911621 300.3333435058594 28.33333396911621 L299.3333435058594 28.33333396911621 C298.7810363769531 28.33333396911621 298.3333435058594 27.885618209838867 298.3333435058594 27.33333396911621 L298.3333435058594 23.33333396911621 C298.3333435058594 22.781047821044922 298.7810363769531 22.33333396911621 299.3333435058594 22.33333396911621 Z M304.0 20.0 L305.0 20.0 C305.5522766113281 20.0 306.0 20.447715759277344 306.0 21.0 L306.0 27.33333396911621 C306.0 27.885618209838867 305.5522766113281 28.33333396911621 305.0 28.33333396911621 L304.0 28.33333396911621 C303.4477233886719 28.33333396911621 303.0 27.885618209838867 303.0 27.33333396911621 L303.0 21.0 C303.0 20.447715759277344 303.4477233886719 20.0 304.0 20.0 Z M308.6666564941406 17.66666603088379 L309.6666564941406 17.66666603088379 C310.2189636230469 17.66666603088379 310.6666564941406 18.114381790161133 310.6666564941406 18.66666603088379 L310.6666564941406 27.33333396911621 C310.6666564941406 27.885618209838867 310.2189636230469 28.33333396911621 309.6666564941406 28.33333396911621 L308.6666564941406 28.33333396911621 C308.1143798828125 28.33333396911621 307.6666564941406 27.885618209838867 307.6666564941406 27.33333396911621 L307.6666564941406 18.66666603088379 C307.6666564941406 18.114381790161133 308.1143798828125 17.66666603088379 308.6666564941406 17.66666603088379 L308.6666564941406 17.66666603088379 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_55-3c7bb" fill="#000000" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_34" class="richtext autofit firer ie-background commentable non-processed" customid="Time"   datasizewidth="35.1px" datasizeheight="18.0px" dataX="28.8" dataY="15.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_34_0">10:42</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_20" class="group firer ie-background commentable non-processed" customid="bottom menu" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_5" class="rectangle manualfit firer commentable non-processed" customid="Rectangle 5"   datasizewidth="375.0px" datasizeheight="86.0px" datasizewidthpx="375.0" datasizeheightpx="86.0" dataX="0.0" dataY="726.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_5_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="notification icon"   datasizewidth="22.2px" datasizeheight="22.1px" dataX="315.0" dataY="745.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="22.191192626953125" height="22.078994750976562" viewBox="315.0 744.9605026245117 22.191192626953125 22.078994750976562" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-3c7bb" d="M336.3189855175048 761.578098022454 C334.3242389771126 759.8530479793582 332.4782806737445 757.4191976674206 332.4782806737445 751.342607204227 C332.4782806737445 747.8234856331751 329.6152009141005 744.9605026245117 326.095982592068 744.9605026245117 C322.57628014585396 744.9605026245117 319.712813013009 747.8234856331751 319.712813013009 751.342607204227 C319.712813013009 757.4247154275433 317.8675321511444 759.8561454878516 315.87104316990496 761.5797435276813 C315.31751763125135 762.0618278145236 315.0 762.7561069428683 315.0 763.4846551208309 C315.0 763.8856179571416 315.325068362122 764.2106862961837 315.7260311753527 764.2106862961837 L322.61606732487314 764.2106862961837 C322.9521713457201 765.8237339698438 324.38448562574536 767.0394973754883 326.09559595742417 767.0394973754883 C327.806706289103 767.0394973754883 329.23911729702894 765.823830813144 329.5751245899752 764.2106862961837 L336.46516073949556 764.2106862961837 C336.8661235758062 764.2106862961837 337.19119191484833 763.8856179340617 337.19119191484833 763.4846551208309 C337.19119191484833 762.7502029173802 336.87289891403753 762.0548587896918 336.3189855175048 761.578098022454 Z M326.09559595742417 765.5874350247827 C325.19144511894706 765.5874350247827 324.4188512417046 765.0134831892285 324.1226303892204 764.2106862961837 L328.0685611563494 764.2106862961837 C327.77234067314373 765.0134830969089 326.9997464266227 765.5874350247827 326.09559595742417 765.5874350247827 Z M316.7380218754283 762.7586239454781 C316.7644494103958 762.7299699147217 316.79261942083036 762.7026711420206 316.8223382951412 762.6768244302855 C319.8661514715 760.048978800998 321.1647793224411 756.6592845617719 321.1647793224411 751.3426065810694 C321.1647793224411 748.6241521813339 323.3769478422382 746.4125643520597 326.0958861815161 746.4125643520597 C328.8144373322323 746.4125643520597 331.0261219124871 748.6241521813339 331.0261219124871 751.3426065810694 C331.0261219124871 756.6544444277919 332.3249432653894 760.0440419160375 335.37030519599494 762.6775024257068 C335.4000240731908 762.7031555258511 335.42819408074035 762.7301638898209 335.45471841861826 762.7586243089868 L316.7380218754283 762.7586243089868 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Path_2" class="path firer commentable non-processed" customid="favorite icon"   datasizewidth="24.4px" datasizeheight="21.5px" dataX="214.2" dataY="746.1"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="24.69999885559082" height="21.83045768737793" viewBox="214.15000006801077 746.0909457500736 24.69999885559082 21.83045768737793" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_2-3c7bb" d="M235.5921772877475 748.3929050058906 C232.99569537645309 746.2097033457109 228.9841740755818 746.5376327998758 226.4999967689438 749.0667326338292 C224.0158187768529 746.5376327998758 220.00429850416097 746.2052111872291 217.40781590741366 748.3929050058906 C214.02969345075712 751.2364573901557 214.52383371124543 755.8723916760091 216.93164445221566 758.3296162888013 L224.8109352407299 766.3571492925525 C225.2601536593556 766.8153520709826 225.86210632317778 767.0714065610312 226.49999642621734 767.0714065610312 C227.14237877342032 767.0714065610312 227.73983927876068 766.8198442508848 228.1890576117048 766.3616414510343 L236.06834908567194 758.3341087471687 C238.4716676253196 755.8768839630133 238.97479211708986 751.2409501912496 235.59217754479232 748.3929050058906 Z M234.53202180265444 756.8157503551231 L226.65273075709538 764.8432833588745 C226.54491833234113 764.9510957836287 226.45507464861598 764.9510957836287 226.34726222386172 764.8432833588745 L218.4679713068251 756.8157503551231 C216.82832407884118 755.1446578035627 216.49590238051283 751.9821602392556 218.79590076099004 750.046028923524 C220.54336047798944 748.5770846603451 223.23867098974384 748.797201754017 224.9277321752313 750.517708263081 L226.4999966404214 752.1214180518475 L228.07226110561146 750.517708263081 C229.7703066937441 748.7882173513718 232.4656173768617 748.5770846603451 234.2040925198527 750.0415367222014 C236.49959857048492 751.977668037933 236.15819264087466 755.1581345788937 234.53202197401765 756.815750612168 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-3c7bb" fill="#26315F" fill-opacity="1.0" stroke-width="0.7" stroke="#FFFFFF" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_21" class="group firer ie-background commentable non-processed" customid="order icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_12" class="path firer commentable non-processed" customid="Path 3"   datasizewidth="4.4px" datasizeheight="3.3px" dataX="136.0" dataY="751.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="4.4110260009765625" height="3.25775146484375" viewBox="136.0000014305115 751.0000059604645 4.4110260009765625 3.25775146484375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_12-3c7bb" d="M140.27427604291876 751.1395229919602 L140.27427604291876 751.1395229919602 C140.08824538697175 750.9534923360133 139.79148219269425 750.9534923360133 139.60545149450613 751.1395229919602 L137.625553675364 753.12385004505 L136.80170357328868 752.2999999429745 C136.71311754262427 752.2114139123101 136.59352640967558 752.1626915996688 136.4695059582972 752.1626915996688 C136.34548550691883 752.1626915996688 136.2258943739701 752.2114139175903 136.1373083433057 752.2999999429745 C136.04872231264133 752.3885859736389 136.0 752.5081771065877 136.0 752.632197557966 C136.0 752.7562179987841 136.04872231792146 752.8758091422931 136.1373083433057 752.9643951729576 L137.2933560603725 754.1204428900244 C137.3819420910369 754.2090289206888 137.50153322398563 754.25775123333 137.625553675364 754.25775123333 C137.7495741161821 754.25775123333 137.86916525969107 754.2090289154087 137.9621805876646 754.1204428900244 L140.27870525574562 751.8039182219433 C140.45587767612386 751.6267461606144 140.45587767612386 751.3255535211839 140.27427604291876 751.1395229919602 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_12-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_13" class="path firer commentable non-processed" customid="Path 4"   datasizewidth="4.4px" datasizeheight="3.3px" dataX="136.0" dataY="756.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="4.4110260009765625" height="3.25775146484375" viewBox="136.0000014305115 756.0000059604645 4.4110260009765625 3.25775146484375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_13-3c7bb" d="M140.27427604291876 756.1395229919602 L140.27427604291876 756.1395229919602 C140.08824538697175 755.9534923360131 139.79148219269425 755.9534923360131 139.60545149450613 756.1395229919602 L137.625553675364 758.1238500450498 L136.80170357328868 757.2999999429745 C136.7086882453152 757.2069846150009 136.58909709124592 757.1626915996687 136.4695059582972 757.1626915996687 C136.3499148147882 757.1626915996687 136.2258943739701 757.2069846150009 136.1373083433057 757.2999999429745 C136.04872231264133 757.3885859736389 136.0 757.5081771065876 136.0 757.632197557966 C136.0 757.7562179987841 136.04872231792146 757.8758091422931 136.1373083433057 757.9643951729574 L137.2933560603725 759.1204428900244 C137.3819420910369 759.2090289206888 137.50153322398563 759.25775123333 137.625553675364 759.25775123333 C137.7495741161821 759.25775123333 137.86916525969107 759.2090289154086 137.9621805876646 759.1204428900244 L140.27870525574562 756.8039182219431 C140.45587767612386 756.6223165887382 140.45587767612386 756.3255541970417 140.27427604291876 756.1395229919602 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_13-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_14" class="path firer commentable non-processed" customid="Path 5"   datasizewidth="5.1px" datasizeheight="1.0px" dataX="142.0" dataY="751.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="5.09368896484375" height="1.0276031494140625" viewBox="142.00000953674316 751.0 5.09368896484375 1.0276031494140625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_14-3c7bb" d="M146.57989793741723 751.0 L142.51379899474995 751.0 C142.23032369239974 751.0 142.0 751.2303236712792 142.0 751.5137989947499 C142.0 751.7972743182206 142.23032367127922 752.0275979894998 142.51379899474995 752.0275979894998 L146.57989793741723 752.0275979894998 C146.86337323976738 752.0275979894998 147.09369693216718 751.7972743182206 147.09369693216718 751.5137989947499 C147.09369693216718 751.2303236712792 146.8633729652002 751.0 146.57989793741723 751.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_14-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_15" class="path firer commentable non-processed" customid="Path 7"   datasizewidth="4.4px" datasizeheight="3.3px" dataX="136.0" dataY="761.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="4.413238525390625" height="3.262176513671875" viewBox="135.99999833106995 761.0000059604645 4.413238525390625 3.262176513671875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_15-3c7bb" d="M140.2764906915733 761.1395229919602 C140.09046003562628 760.9534923360131 139.79369684134878 760.9534923360131 139.60766614316066 761.1395229919602 L137.62776832401855 763.1238500450498 L136.8039182219432 762.2999999429744 C136.71533219127883 762.21141391231 136.5957410583301 762.1626915996687 136.47172060695172 762.1626915996687 C136.34770015557336 762.1626915996687 136.22810902262464 762.2114139175902 136.13952299196026 762.2999999429744 C135.95349233601326 762.4860305989215 135.95349233601326 762.7827937931991 136.13952299196026 762.9688244913871 L137.29557070902706 764.124872208454 C137.38415673969143 764.2134582391184 137.50374787264016 764.2621805517597 137.62776832401855 764.2621805517597 C137.75178876483665 764.2621805517597 137.87137990834563 764.2134582338382 137.96439523631912 764.124872208454 L140.28091990440018 761.8083475403728 C140.45809232477842 761.6223165887382 140.45809232477842 761.3211246251653 140.2764906915733 761.1395229919602 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_15-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_16" class="path firer commentable non-processed" customid="Path 18"   datasizewidth="5.1px" datasizeheight="1.0px" dataX="142.0" dataY="762.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="5.09368896484375" height="1.027587890625" viewBox="142.00000953674316 762.0 5.09368896484375 1.027587890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_16-3c7bb" d="M146.57989793741723 762.0 L142.51379899474995 762.0 C142.2303236923998 762.0 142.0 762.2303236712792 142.0 762.51379899475 C142.0 762.7972742971001 142.23032367127922 763.0275979894999 142.51379899474995 763.0275979894999 L146.57989793741723 763.0275979894999 C146.86337323976744 763.0275979894999 147.09369693216718 762.7972743182206 147.09369693216718 762.51379899475 C147.09369718561385 762.2303242204136 146.8633729652002 762.0 146.57989793741723 762.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_16-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_17" class="path firer commentable non-processed" customid="Path 19"   datasizewidth="6.2px" datasizeheight="1.0px" dataX="142.0" dataY="757.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="6.15673828125" height="1.027587890625" viewBox="141.9999942779541 757.0 6.15673828125 1.027587890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_17-3c7bb" d="M147.64292962953215 757.0 L142.5137989947499 757.0 C142.23032369239974 757.0 142.0 757.2303236712792 142.0 757.51379899475 C142.0 757.7972742971001 142.23032367127917 758.0275979894999 142.5137989947499 758.0275979894999 L147.64293030538994 758.0275979894999 C147.92640560774015 758.0275979894999 148.1567293001399 757.7972743182206 148.1567293001399 757.51379899475 C148.15672887772877 757.2347537922899 147.92640465731512 757.0 147.64292962953215 757.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_17-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_18" class="path firer commentable non-processed" customid="Path 20"   datasizewidth="18.6px" datasizeheight="26.0px" dataX="133.0" dataY="743.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="18.55877685546875" height="26.0" viewBox="133.0 742.9999992847443 18.55877685546875 26.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_18-3c7bb" d="M151.5587734241908 747.1413969018796 C151.5587734241908 746.4194207857577 150.97410563025403 745.8347529495798 150.25212947189095 745.8347529495798 L147.30664395229982 745.8347529495798 L147.30664395229982 745.0153321659342 C147.30664395229982 744.6875638482518 147.03645656299753 744.4173764589494 146.70868824531516 744.4173764589494 L144.20613287904598 744.4173764589494 L144.17955706879064 744.3420783307727 C143.89165246913137 743.5403747363634 143.12981257162468 743.0 142.2793866434536 743.0 C141.42896071528253 743.0 140.66269158382838 743.5403747786046 140.37921621811654 744.3420783307727 L140.3526404078612 744.4173764589494 L137.85008517887564 744.4173764589494 C137.52231686119325 744.4173764589494 137.25212947189095 744.6875638482518 137.25212947189095 745.0153321659342 L137.25212947189095 745.8347529495798 L134.30664395229982 745.8347529495798 C133.5846678361779 745.8347529495798 133.0 746.4194207435166 133.0 747.1413969018796 L133.0 767.6933560160193 C133.0 768.4153321321412 133.58466779393677 768.9999999683191 134.30664395229982 768.9999999683191 L150.25212947189095 768.9999999683191 C150.9741055880129 768.9999999683191 151.5587734241908 768.4153321743823 151.5587734241908 767.6933560160193 L151.5587734241908 747.1413969018796 Z M138.4480408858603 745.6132878729188 L140.86201022146508 745.6132878729188 C141.18977853914745 745.6132878729188 141.45996592844975 745.3431004836166 141.45996592844975 745.0153321659342 C141.45996592844975 744.563543417994 141.8275979641552 744.1959113822885 142.2793867120954 744.1959113822885 C142.7311754600356 744.1959113822885 143.09880749574106 744.563543417994 143.09880749574106 745.0153321659342 C143.09880749574106 745.3431004836166 143.36899488504335 745.6132878729188 143.69676320272572 745.6132878729188 L146.1107325383305 745.6132878729188 L146.1107325383305 747.2521294402102 L138.4480408858603 747.2521294402102 L138.4480408858603 745.6132878729188 Z M150.36286201022148 767.8040885543497 L134.19591141396933 767.8040885543497 L134.19591141396933 747.0306643635491 L137.25212947189095 747.0306643635491 L137.25212947189095 747.8500851471948 C137.25212947189095 748.1778534648772 137.52231686119325 748.4480408541795 137.85008517887564 748.4480408541795 L146.70868824531516 748.4480408541795 C147.03645656299753 748.4480408541795 147.30664395229982 748.1778534648772 147.30664395229982 747.8500851471948 L147.30664395229982 747.0306643635491 L150.36286201022148 747.0306643635491 L150.36286201022148 767.8040885543497 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_18-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_22" class="group firer ie-background commentable non-processed" customid="home icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_19" class="path firer commentable non-processed" customid="Path 1"   datasizewidth="24.7px" datasizeheight="9.8px" dataX="44.0" dataY="746.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="24.670612335205078" height="9.844337463378906" viewBox="44.000000000832195 745.9999998911279 24.670612335205078 9.844337463378906" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_19-3c7bb" d="M68.40822671998252 754.6860864336766 L56.71399425969352 746.1238105723689 C56.48848988574291 745.9587297945569 56.182131568442735 745.9587297945569 55.9567235798777 746.1238105723689 L44.262393539437916 754.6860864336766 C43.97675465657663 754.8952562074567 43.91469276162182 755.2963456200526 44.12386253540192 755.5819845258902 C44.33303230918202 755.8676234317278 44.73416992595896 755.929588941297 45.01976062761558 755.7205155299262 L56.33531030203213 747.4354462432808 L67.65086027513996 755.7204672797927 C67.76496163923906 755.8040677298742 67.89756591855237 755.8443019480101 68.02901378002497 755.8443019480101 C68.22637830854066 755.8443019480101 68.42114082224187 755.753473794519 68.54671018614897 755.5819362642686 C68.75592765863239 755.2963460336252 68.69386456891237 754.8952563682905 68.40822671998252 754.6860864336766 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_19-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_20" class="path firer commentable non-processed" customid="Path 2"   datasizewidth="19.2px" datasizeheight="12.1px" dataX="46.7" dataY="755.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="19.23041534423828" height="12.12615966796875" viewBox="46.72009440512028 755.0 19.23041534423828 12.12615966796875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_20-3c7bb" d="M65.30946354244544 755.0 C64.95549874849776 755.0 64.66846249581991 755.2869880484969 64.66846249581991 755.6410010466255 L64.66846249581991 765.8440621223847 L59.540550393320046 765.8440621223847 L59.540550393320046 760.2745862126258 C59.540550393320046 758.5071715099068 58.102622709359835 757.0693403262135 56.335304506907875 757.0693403262135 C54.56798630445594 757.0693403262135 53.13005862049573 758.5072680101738 53.13005862049573 760.2745862126258 L53.13005862049573 765.8441106941857 L48.002098405719956 765.8441106941857 L48.002098405719956 755.6410492508064 C48.002098405719956 755.2870362756541 47.715062176018364 755.0000482041809 47.36109735909443 755.0000482041809 C47.0071325421705 755.0000482041809 46.72009631246891 755.2870362526778 46.72009631246891 755.6410492508064 L46.72009631246891 766.4851595773721 C46.72009631246891 766.8391725525244 47.0071325421705 767.1261606239976 47.36109735909443 767.1261606239976 L53.77105975902627 767.1261606239976 C54.10815989062334 767.1261606239976 54.384017276673674 766.8657223263003 54.40955519919916 766.5350789356535 C54.41109710957819 766.5200934953746 54.412060803497766 766.5037106967673 54.412060803497766 766.4851595888601 L54.412060803497766 760.2746340147221 C54.412060803497766 759.2141369797195 55.27480783737121 758.351389945846 56.335304872373925 758.351389945846 C57.39580190737662 758.351389945846 58.25854894125008 759.2141851379479 58.25854894125008 760.2746340147221 L58.25854894125008 766.4851595888601 C58.25854894125008 766.5036143271778 58.25951263516967 766.5197080170169 58.26105454554869 766.5345007209892 C58.286303326277576 766.8653850406355 58.56225708335333 767.1261606297417 58.89954998572159 767.1261606297417 L65.30951238565342 767.1261606297417 C65.66352536080575 767.1261606297417 65.95051343227895 766.8391725812447 65.95051343227895 766.4851595831161 L65.95051343227895 755.6410492508064 C65.95046500264353 755.2869877498056 65.66347578235764 755.0 65.30946354244544 755.0 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_20-3c7bb" fill="#26315F" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Hotspot_6" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot 5"   datasizewidth="28.0px" datasizeheight="24.0px" dataX="42.3" dataY="746.0"  >\
            <div class="clickableSpot"></div>\
          </div>\
        </div>\
\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;