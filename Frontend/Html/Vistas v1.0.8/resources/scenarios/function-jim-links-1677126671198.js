(function(window, undefined) {

  var jimLinks = {
    "b20b41a2-1830-489f-b3ec-907b5c55442b" : {
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_3" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ]
    },
    "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d" : {
      "Hotspot_11" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ],
      "Hotspot_10" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ],
      "Hotspot_9" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ],
      "Hotspot_8" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ],
      "Hotspot_7" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ],
      "Hotspot_5" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ],
      "Hotspot_6" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ]
    },
    "848251df-7b33-4931-8c53-144ed897ff21" : {
      "Button_2" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ],
      "Hotspot_1" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ]
    },
    "8d1c6733-a7e9-4eca-9ab2-1f5297476b92" : {
      "Button_1" : [
        "848251df-7b33-4931-8c53-144ed897ff21"
      ],
      "Hotspot_1" : [
        "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0"
      ]
    },
    "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0" : {
      "Button_2" : [
        "8d1c6733-a7e9-4eca-9ab2-1f5297476b92"
      ],
      "Hotspot_2" : [
        "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d"
      ],
      "Hotspot_1" : [
        "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d"
      ]
    },
    "97d2b188-bcf3-4297-ab84-fd191db0eec5" : {
      "Hotspot_2" : [
        "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d"
      ],
      "Hotspot_3" : [
        "97d2b188-bcf3-4297-ab84-fd191db0eec5"
      ],
      "Hotspot_1" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Hotspot_1" : [
        "ed6b85e8-10f9-4d25-9976-358166e8fcbb"
      ],
      "Hotspot_2" : [
        "b20b41a2-1830-489f-b3ec-907b5c55442b"
      ]
    },
    "ed6b85e8-10f9-4d25-9976-358166e8fcbb" : {
      "Hotspot_2" : [
        "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d"
      ],
      "Hotspot_1" : [
        "97d2b188-bcf3-4297-ab84-fd191db0eec5"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);