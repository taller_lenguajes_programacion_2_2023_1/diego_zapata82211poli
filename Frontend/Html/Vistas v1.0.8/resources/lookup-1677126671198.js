(function(window, undefined) {
  var dictionary = {
    "b20b41a2-1830-489f-b3ec-907b5c55442b": "Registrarse",
    "3c7bb3f0-1584-4b54-9478-0503c8fb7b3d": "Menu",
    "848251df-7b33-4931-8c53-144ed897ff21": "Succesfully order",
    "8d1c6733-a7e9-4eca-9ab2-1f5297476b92": "Checkout",
    "4f9fd77c-bae8-4fda-aa9d-c518459d6ba0": "Order",
    "97d2b188-bcf3-4297-ab84-fd191db0eec5": "Filtros",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Login",
    "ed6b85e8-10f9-4d25-9976-358166e8fcbb": "Descubre",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);