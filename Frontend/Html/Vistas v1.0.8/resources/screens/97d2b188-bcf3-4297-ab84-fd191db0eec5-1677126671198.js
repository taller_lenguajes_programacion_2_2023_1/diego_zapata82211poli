jQuery("#simulation")
  .on("click", ".s-97d2b188-bcf3-4297-ab84-fd191db0eec5 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Hotspot_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/3c7bb3f0-1584-4b54-9478-0503c8fb7b3d",
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/97d2b188-bcf3-4297-ab84-fd191db0eec5",
                    "transition": {
                      "type": "turn",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ed6b85e8-10f9-4d25-9976-358166e8fcbb",
                    "transition": {
                      "type": "slidedown",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("click", ".s-97d2b188-bcf3-4297-ab84-fd191db0eec5 .toggle", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Button_4")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimChangeStyle",
                    "parameter": [ {
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_4 > .backgroundLayer > .colorLayer" ],
                      "attributes": {
                        "background-color": "#369361"
                      }
                    },{
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_4 span" ],
                      "attributes": {
                        "color": "#FFFFFF"
                      }
                    },{
                      "effect": {
                        "type": "none",
                        "easing": "easeOutCubic",
                        "duration": 500
                      }
                    } ],
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    } else if(jFirer.is("#s-Button_3")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimChangeStyle",
                    "parameter": [ {
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_3 > .backgroundLayer > .colorLayer" ],
                      "attributes": {
                        "background-color": "#369361"
                      }
                    },{
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_3 span" ],
                      "attributes": {
                        "color": "#FFFFFF"
                      }
                    },{
                      "effect": {
                        "type": "none",
                        "easing": "easeOutCubic",
                        "duration": 500
                      }
                    } ],
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    } else if(jFirer.is("#s-Button_2")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimChangeStyle",
                    "parameter": [ {
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_2 > .backgroundLayer > .colorLayer" ],
                      "attributes": {
                        "background-color": "#369361"
                      }
                    },{
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_2 span" ],
                      "attributes": {
                        "color": "#FFFFFF"
                      }
                    },{
                      "effect": {
                        "type": "none",
                        "easing": "easeOutCubic",
                        "duration": 500
                      }
                    } ],
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    } else if(jFirer.is("#s-Button_1")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimChangeStyle",
                    "parameter": [ {
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_1 > .backgroundLayer > .colorLayer" ],
                      "attributes": {
                        "background-color": "#369361"
                      }
                    },{
                      "target": [ "#s-97d2b188-bcf3-4297-ab84-fd191db0eec5 #s-Button_1 span" ],
                      "attributes": {
                        "color": "#FFFFFF"
                      }
                    },{
                      "effect": {
                        "type": "none",
                        "easing": "easeInOutCubic",
                        "duration": 500
                      }
                    } ],
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    }
  });