export interface ProductModel {
    name: string;
    id: string;
    price: string;
    description: string;
}