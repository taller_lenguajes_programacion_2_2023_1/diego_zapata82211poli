import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, NgModel, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterModel } from 'src/app/shared/models/user.interface';
import { UserService } from 'src/app/user.service';

declare var window: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(private router: Router, private readonly loginService: UserService) { }


  public loginForm: FormGroup = new FormGroup({

    correo: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$'),
      Validators.maxLength(30)]),
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(25)]),
  });

  public errorMessages = {
    'correo': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El campo es numerico.' },
      { type: 'maxlength', message: 'Maximo es de 25 caracteres.' },
    ],
    'password': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'minlength', message: 'Minimo es de 5 caracteres.' },
      { type: 'maxlength', message: 'Maximo es de 25 caracteres.' },
    ]
  };

  ngOnInit(): void {
  }

  public submit(): void {
    let fount = false;
    this.loginService.getAll().subscribe({
      next: (backResponse) => {
        const dataLogin = this.loginForm.value;
        const mailLogin = dataLogin.correo;
        const passLogin = dataLogin.password;
        backResponse.forEach(register => {
          if (register.nick === mailLogin) {
            if (register.pass === passLogin) {
              fount = true;
              sessionStorage.setItem('token', '1');
              this.router.navigate(['/dashboard']);
              alert('Se ha iniciado sesión con el usuario de ' + mailLogin);
            } else {
              alert('La contraseña ingresada es incorrecta');
            }
          }
        });
      }
    });

    if (fount) {
      alert('El usuario no existe');
    }

    // if (pass === passLogin) {
    //   sessionStorage.setItem("userInfo", JSON.stringify(userExists));
    //   sessionStorage.setItem('token', '1');
    //   this.router.navigate(['/dashboard']);
    //   alert('Se ha iniciado sesión con el usuario de ' + mailLogin);
    // } else {
    //   alert('La contraseña ingresada es incorrecta')
    // }

  }
}
