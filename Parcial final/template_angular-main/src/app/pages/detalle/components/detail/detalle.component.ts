import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductModel } from 'src/app/shared/models/product.interface';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss']
})


export class DetalleComponent implements OnInit {
  public indice: any;
  public listProducts: any;
  public producto: any;
  cantidad = 1;

  constructor(private route: ActivatedRoute, private productsService: ProductsService) { };

  ngOnInit(): void {
    
    this.indice = this.route.snapshot.params['id'];
    // this.callProducts();
    this.findProduct(this.indice);
  }

  // public callProducts() {
  //   this.productsService.getAllProducts().subscribe(
  //     data => {
  //       this.listProducts = data
  //       console.log('Este es el indice ' + this.indice);
  //       console.log('este es listProducts ', this.listProducts);
  //       return data;
  //     }
  //   );
  // }
  public findProduct(indice: number) {
    this.productsService.getAllProducts().subscribe({
      next: (backResponse) => {
        backResponse.find(product => {
          console.log('product ', product);
          console.log('product.id ', product.id, this.indice)
          if (product.id == this.indice) {
            this.producto = product
            console.log('producto verdadero1: ',this.producto);
          }
        });
      }
    });
  }

  aumentarCantidad() {
    this.cantidad++;
  }

  disminuirCantidad() {
    if (this.cantidad > 1) {
      this.cantidad--;
    }
  }
}

