import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { Login } from '../../interfaces/login';

export interface User{
		id: number,
		email: String,
		first_name: String,
		last_name: String
}

@Injectable()
export class AuthService {
  
  private headers: HttpHeaders = new HttpHeaders({'Content-type': 'application/json'});

  constructor(
		public _http: HttpClient,
		private router: Router
	) { }
}
