import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as data from './data.json';
import { of, Observable } from 'rxjs';
import { ProductModel } from 'src/app/shared/models/product.interface';

@Injectable()
export class ProductsService {
	private readonly url = "http://localhost:9001/market/products";
	constructor(private readonly http: HttpClient) { }

	public getAllProducts(){
		return this.http.get<ProductModel[]>(this.url);
	}
}
